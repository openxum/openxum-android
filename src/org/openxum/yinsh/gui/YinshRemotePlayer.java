package org.openxum.yinsh.gui;

import org.openxum.common.Color;

public class YinshRemotePlayer {
    
    public YinshRemotePlayer(String name, Color color, int id)
    {
	this.name = name;
	this.color = color;
	this.id = id;
    }
    
    public Color getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public int getID() {
        return id;
    }

    private String name;
    private Color color;
    private int id;
}
