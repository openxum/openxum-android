/**
 * @file org/openxum/yinsh/gui/BoardView.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.yinsh.gui;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map.Entry;

import org.openxum.common.InvalidMovingException;
import org.openxum.common.OpenxumException;
import org.openxum.yinsh.game.*;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.MotionEvent;

public class BoardView extends org.openxum.gui.BoardView implements
	OnTouchListener {
    
    public BoardView(Context context) {
	super(context);
	init();
    }

    public BoardView(Context context, AttributeSet attrs) {
	super(context, attrs);
	init();
    }

    public BoardView(Context context, AttributeSet attrs, int defStyle) {
	super(context, attrs, defStyle);
	init();
    }

    protected void computeCoordinates(char letter, int number, Point pt) {
	pt.x = offset_x + delta_x * (letter - 'A');
	pt.y = offset_y + 7 * delta_y + delta_xy * (letter - 'A')
		- (number - 1) * delta_y;
    }

    private void computeDeltas() {
	int base = (getHeight() < getWidth()) ? getHeight() : getWidth();

	offset_x = (int) ((base / 9.) / 2);
	offset_y = offset_x;
	delta_x = (int) ((base - 2 * offset_x) / 9.) - 2;
	delta_y = delta_x;
	delta_xy = delta_y / 2;
	if (getHeight() < getWidth()) {
	    offset_x += (getWidth() - 11 * delta_x) / 2;
	} else {
	    offset_y += (getHeight() - 12 * delta_y) / 2;
	}
    }

    private void drawGrid(Canvas canvas) {
	Paint paint = new Paint();

	paint.setColor(Color.DKGRAY);
	paint.setStrokeWidth(2);
	for (char l = 'A'; l < 'L'; ++l) {
	    Point pt1 = new Point();
	    Point pt2 = new Point();

	    computeCoordinates(l,
		    org.openxum.yinsh.game.Board.BEGIN_NUMBER[l - 'A'], pt1);
	    computeCoordinates(l,
		    org.openxum.yinsh.game.Board.END_NUMBER[l - 'A'], pt2);

	    canvas.drawLine(pt1.x, pt1.y, pt2.x, pt2.y, paint);
	}

	for (int n = 1; n < 12; ++n) {
	    Point pt1 = new Point();
	    Point pt2 = new Point();

	    computeCoordinates(
		    org.openxum.yinsh.game.Board.BEGIN_LETTER[n - 1], n, pt1);
	    computeCoordinates(org.openxum.yinsh.game.Board.END_LETTER[n - 1],
		    n, pt2);

	    canvas.drawLine(pt1.x, pt1.y, pt2.x, pt2.y, paint);
	}

	for (int i = 0; i < 11; ++i) {
	    Point pt1 = new Point();
	    Point pt2 = new Point();

	    computeCoordinates(
		    org.openxum.yinsh.game.Board.BEGIN_DIAGONAL_LETTER[i],
		    org.openxum.yinsh.game.Board.BEGIN_DIAGONAL_NUMBER[i], pt1);
	    computeCoordinates(
		    org.openxum.yinsh.game.Board.END_DIAGONAL_LETTER[i],
		    org.openxum.yinsh.game.Board.END_DIAGONAL_NUMBER[i], pt2);

	    canvas.drawLine(pt1.x, pt1.y, pt2.x, pt2.y, paint);
	}
    }

    private void drawCoordinates(Canvas canvas) {
	Paint paint = new Paint();
	String txt;

	paint.setTextSize(16);
	paint.setTypeface(Typeface.DEFAULT_BOLD);
	// letters
	for (char l = 'A'; l < 'L'; ++l) {
	    Point pt = new Point();

	    txt = "" + l;
	    computeCoordinates(l,
		    org.openxum.yinsh.game.Board.BEGIN_NUMBER[l - 'A'], pt);
	    pt.x -= 5;
	    pt.y += 20;

	    canvas.drawText(txt, pt.x, pt.y, paint);
	}

	// numbers
	for (int n = 1; n < 12; ++n) {
	    Point pt = new Point();

	    if (n < 10) {
		txt = "" + (char) ('0' + n);
	    } else {
		txt = "1" + (char) ('0' + (n - 10));
	    }
	    computeCoordinates(
		    org.openxum.yinsh.game.Board.BEGIN_LETTER[n - 1], n, pt);
	    pt.x -= 15 + (n > 9 ? 5 : 0);
	    pt.y -= 3;

	    canvas.drawText(txt, pt.x, pt.y, paint);
	}
    }

    private void drawMarker(Canvas canvas, Point pt, int color) {
	Paint paint = new Paint();

	paint.setStyle(Paint.Style.FILL);
	if (color == Color.BLACK) {
	    paint.setColor(Color.BLACK);
	    canvas.drawCircle(pt.x, pt.y,
		    (float) (delta_x * (1. / 3 - 1. / 10) - 1), paint);
	} else if (color == Color.WHITE) {
	    paint.setColor(Color.WHITE);
	    canvas.drawCircle(pt.x, pt.y,
		    (float) (delta_x * (1. / 3 - 1. / 10) - 1), paint);
	    paint.setColor(Color.BLACK);
	    paint.setStyle(Paint.Style.STROKE);
	    canvas.drawCircle(pt.x, pt.y,
		    (float) (delta_x * (1. / 3 - 1. / 10) - 1), paint);
	}
    }

    private void drawPossibleMoving(Canvas canvas) {
	try {
	    PossibleMovingList list = activity.getPossibleMovingList(
		    selectedRing, selectedColor, true);
	    ListIterator<Intersection> it = list.listIterator();

	    while (it.hasNext()) {
		Intersection intersection = it.next();
		Point pt = new Point();
		Paint paint = new Paint();

		computeCoordinates(intersection.getLetter(),
			intersection.getNumber(), pt);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.BLUE);
		canvas.drawCircle(pt.x, pt.y, 5, paint);
	    }
	} catch (InvalidMovingException e) {
	}
    }

    private void drawRing(Canvas canvas, Point pt,
	    org.openxum.common.Color color, boolean selected) {
	super.drawRing(canvas, pt, (float) delta_x, color, selected);
    }

    private void drawRows(Canvas canvas) {
	if (activity.getPhase() == Phase.REMOVE_ROW
		|| activity.getPhase() == Phase.REMOVE_ROW_OTHER) {
	    SeparatedRows srows = activity.getRows(selectedColor);
	    ListIterator<Rows> its = srows.listIterator();

	    while (its.hasNext()) {
		Rows rows = its.next();
		ListIterator<Row> itr = rows.listIterator();

		while (itr.hasNext()) {
		    Row row = itr.next();
		    Coordinates begin = row.firstElement();
		    Coordinates end = row.lastElement();
		    Point pt1 = new Point();
		    Point pt2 = new Point();
		    double alpha_1 = 0;
		    double beta_1 = 0;
		    double alpha_2 = 0;
		    double beta_2 = 0;

		    computeCoordinates(begin.getLetter(), begin.getNumber(),
			    pt1);
		    computeCoordinates(end.getLetter(), end.getNumber(), pt2);

		    if (pt1.x == pt2.x) {
			if (pt1.y < pt2.y) {
			    alpha_1 = Math.PI;
			    beta_1 = 0;
			    alpha_2 = 0;
			    beta_2 = Math.PI;
			} else {
			    alpha_1 = 0;
			    beta_1 = Math.PI;
			    alpha_2 = Math.PI;
			    beta_2 = 0;
			}
		    } else {
			double omega_1 = Math.acos(1. / Math.sqrt(5));

			if (pt1.x < pt2.x) {
			    if (pt1.y < pt2.y) {
				alpha_1 = Math.PI - omega_1;
				beta_1 = 3 * Math.PI / 2 + omega_1 / 2;
				alpha_2 = 3 * Math.PI / 2 + omega_1 / 2;
				beta_2 = Math.PI - omega_1;
			    } else {
				alpha_1 = omega_1;
				beta_1 = Math.PI + omega_1;
				alpha_2 = Math.PI + omega_1;
				beta_2 = omega_1;
			    }
			}
		    }

		    Paint paint = new Paint();
		    float[] phase = new float[] { 2.f, 2.f };
		    DashPathEffect effect = new DashPathEffect(phase, 0.f);

		    paint.setPathEffect(effect);
		    paint.setColor(Color.GREEN);
		    paint.setStyle(Paint.Style.STROKE);
		    paint.setStrokeWidth(4);

		    int radius = (int) (delta_x / 3 + 5);
		    {
			Rect r = new Rect(pt1.x - radius, pt1.y - radius, pt1.x
				+ radius, pt1.y + radius);
			float startAngle = (float) (180 * alpha_1 / Math.PI);
			float sweepAngle = (float) (180 * beta_1 / Math.PI)
				- startAngle;

			if (sweepAngle < 0) {
			    sweepAngle = -sweepAngle;
			}

			canvas.drawArc(new RectF(r), startAngle, sweepAngle,
				false, paint);

			canvas.drawLine(
				pt1.x + (int) (radius * Math.cos(beta_1)),
				pt1.y + (int) (radius * Math.sin(beta_1)),
				pt2.x + (int) (radius * Math.cos(alpha_2)),
				pt2.y + (int) (radius * Math.sin(alpha_2)),
				paint);
		    }
		    {
			Rect r = new Rect(pt2.x - radius, pt2.y - radius, pt2.x
				+ radius, pt2.y + radius);
			float startAngle = (float) (180 * alpha_2 / Math.PI);
			float sweepAngle = (float) (180 * beta_2 / Math.PI)
				- startAngle;

			if (sweepAngle < 0) {
			    sweepAngle = -sweepAngle;
			}

			canvas.drawArc(new RectF(r), startAngle, sweepAngle,
				false, paint);
			canvas.drawLine(
				pt2.x + (int) (radius * Math.cos(beta_2)),
				pt2.y + (int) (radius * Math.sin(beta_2)),
				pt1.x + (int) (radius * Math.cos(alpha_1)),
				pt1.y + (int) (radius * Math.sin(alpha_1)),
				paint);
		    }
		}

		{
		    Paint paint = new Paint();
		    ListIterator<Coordinates> it = selectedRow.listIterator();

		    paint.setStyle(Paint.Style.FILL);
		    paint.setColor(Color.BLUE);
		    while (it.hasNext()) {
			Coordinates coordinates = it.next();
			Point pt = new Point();

			computeCoordinates(coordinates.getLetter(),
				coordinates.getNumber(), pt);

			int radius = (int) (delta_x * (1. / 3 - 1. / 10) - 1);
			Rect r = new Rect(pt.x - radius, pt.y - radius, pt.x
				+ radius, pt.y + radius);
			canvas.drawArc(new RectF(r), 0, 360, false, paint);
		    }
		}
	    }
	}
    }

    private void drawScore(Canvas canvas) {
	int i;

	{
	    Paint paint = new Paint();
	    int x = (getHeight() < getWidth()) ? offset_x - 20 : 0;
	    int y = (getHeight() < getWidth()) ? 0 : offset_y - 20;
	    int radius = delta_x / 2;
	    int left = (getHeight() < getWidth()) ? x - radius - 5 : 0;
	    int top = (getHeight() < getWidth()) ? 0 : y - radius - 5;
	    int right = (getHeight() < getWidth()) ? x + radius + 5
		    : 6 * radius;
	    int bottom = (getHeight() < getWidth()) ? 6 * radius : y + radius
		    + 5;

	    paint.setColor(Color.WHITE);
	    canvas.drawRect(left, top, right, bottom, paint);
	}

	for (i = 0; i < activity
		.getRemovedRingNumber(org.openxum.common.Color.BLACK); ++i) {
	    int x = (getHeight() < getWidth()) ? offset_x - 20
		    : (int) ((i + 0.5) * delta_x);
	    int y = (getHeight() < getWidth()) ? (int) ((i + 0.5) * delta_y)
		    : offset_y - 20;
	    Point pt = new Point(x, y);

	    drawRing(canvas, pt, org.openxum.common.Color.BLACK, false);
	}

	{
	    Paint paint = new Paint();
	    int x = (getHeight() < getWidth()) ? getWidth() - offset_x + 20 : 0;
	    int y = (getHeight() < getWidth()) ? 0 : getHeight() - offset_y
		    + 20;
	    int radius = delta_x / 2;
	    int left = (getHeight() < getWidth()) ? x - radius - 5 : getWidth()
		    - 6 * radius;
	    int top = (getHeight() < getWidth()) ? getHeight() - 6 * radius : y
		    - radius - 5;
	    int right = (getHeight() < getWidth()) ? x + radius + 5
		    : getWidth();
	    int bottom = (getHeight() < getWidth()) ? getHeight() : y + radius
		    + 5;

	    paint.setColor(Color.BLACK);
	    canvas.drawRect(left, top, right, bottom, paint);
	}
	for (i = 0; i < activity
		.getRemovedRingNumber(org.openxum.common.Color.WHITE); ++i) {
	    int x = (getHeight() < getWidth()) ? getWidth() - offset_x + 20
		    : (int) (getWidth() - (i + 0.5) * delta_x);
	    int y = (getHeight() < getWidth()) ? (int) (getHeight() - (i + 0.5)
		    * delta_x) : getHeight() - offset_y + 20;
	    Point pt = new Point(x, y);

	    drawRing(canvas, pt, org.openxum.common.Color.WHITE, false);
	}
    }

    private void drawState(Canvas canvas) {
	Intersections intersections = activity.getIntersections();
	Iterator<Entry<Coordinates, Intersection>> it = intersections
		.entrySet().iterator();

	while (it.hasNext()) {
	    Point pt = new Point();
	    Entry<Coordinates, Intersection> entry = it.next();

	    computeCoordinates(entry.getValue().getLetter(), entry.getValue()
		    .getNumber(), pt);
	    switch (entry.getValue().getState()) {
	    case VACANT:
		break;
	    case BLACK_MARKER:
		drawMarker(canvas, pt, Color.BLACK);
		break;
	    case WHITE_MARKER:
		drawMarker(canvas, pt, Color.WHITE);
		break;
	    case BLACK_MARKER_RING:
		drawMarker(canvas, pt, Color.BLACK);
	    case BLACK_RING:
		if (!selectedRing.isValid()
			|| (selectedRing.isValid() && entry.getKey() != selectedRing)) {
		    drawRing(canvas, pt, org.openxum.common.Color.BLACK, false);
		}
		break;
	    case WHITE_MARKER_RING:
		drawMarker(canvas, pt, Color.WHITE);
	    case WHITE_RING:
		if (!selectedRing.isValid()
			|| (selectedRing.isValid() && entry.getKey() != selectedRing)) {
		    drawRing(canvas, pt, org.openxum.common.Color.WHITE, false);
		}
		break;
	    }
	}
	drawRows(canvas);
    }

    public Coordinates getSelectedCoordinates() {
	return selectedCoordinates;
    }

    public Row getSelectedRow() {
	return selectedRow;
    }

    private void init() {
	selectedRing = new Coordinates('X', 0);
	selectedRow = new Row();

	this.setOnTouchListener(this);
    }

    public void onDraw(Canvas canvas) {
	super.onDraw(canvas);

	computeDeltas();

	Paint paint = new Paint();

	// background
	paint.setStyle(Paint.Style.FILL);
	if (getHeight() < getWidth()) { // landscape
	    paint.setColor(Color.BLACK);
	    canvas.drawRect(0, 0, offset_x - 20, getHeight(), paint);
	    paint.setColor(Color.GRAY);
	    canvas.drawRect(offset_x - 20, 0, getWidth() - offset_x + 20,
		    getHeight(), paint);
	    paint.setColor(Color.WHITE);
	    canvas.drawRect(getWidth() - offset_x + 20, 0, getWidth(),
		    getHeight(), paint);
	} else {
	    paint.setColor(Color.BLACK);
	    canvas.drawRect(0, 0, getWidth(), offset_y - 20, paint);
	    paint.setColor(Color.GRAY);
	    canvas.drawRect(0, offset_y - 20, getWidth(), getHeight()
		    - offset_y + 20, paint);
	    paint.setColor(Color.WHITE);
	    canvas.drawRect(0, getHeight() - offset_y + 20, getWidth(),
		    getHeight(), paint);
	}

	// grid
	drawGrid(canvas);
	drawCoordinates(canvas);

	// state
	drawState(canvas);

	if ((activity.getPhase() == Phase.MOVE_RING || activity.getPhase() == Phase.MOVE_RING_OTHER)
		&& selectedRing.isValid()) {
	    drawPossibleMoving(canvas);
	}

	// score
	drawScore(canvas);
    }

    public boolean onTouch(View v, MotionEvent event) {
	int action = event.getAction();

	if (action == MotionEvent.ACTION_DOWN) {
	    return true;
	} else if (action == MotionEvent.ACTION_UP) {
	    int xTouch = (int) event.getX();
	    int yTouch = (int) event.getY();
	    char letter;
	    int number;

	    letter = computeLetter(xTouch, yTouch);
	    number = computeNumber(xTouch, yTouch);

	    if (letter != 'X' && number != -1
		    && activity.existIntersection(letter, number)) {

		Log.i("OpenXum", "Phase = " + activity.getPhase()
			+ " ; Color = " + activity.getCurrentColor());

		if ((activity.getPhase() == Phase.PUT_RING || activity
			.getPhase() == Phase.PUT_RING_OTHER)
			&& activity.getIntersectionState(letter, number) == State.VACANT) {
		    selectedCoordinates = new Coordinates(letter, number);

		    try {
			if (activity.getPhase() == Phase.PUT_RING) {
			    activity.putRing();
			} else {
			    activity.putRingOther();
			}
		    } catch (OpenxumException e) {
			e.printStackTrace();
		    }
		} else if ((activity.getPhase() == Phase.PUT_MARKER || activity
			.getPhase() == Phase.PUT_MARKER_OTHER)
			&& ((activity.getIntersectionState(letter, number) == State.BLACK_RING && activity
				.getCurrentColor() == org.openxum.common.Color.BLACK) || (activity
				.getIntersectionState(letter, number) == State.WHITE_RING && activity
				.getCurrentColor() == org.openxum.common.Color.WHITE))) {
		    selectedCoordinates = new Coordinates(letter, number);

		    try {
			if (activity.getPhase() == Phase.PUT_MARKER) {
			    activity.putMarker();
			} else {
			    activity.putMarkerOther();
			}
		    } catch (OpenxumException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    }

		    State state = activity.getIntersectionState(letter, number);

		    if (state == State.BLACK_MARKER_RING
			    || state == State.WHITE_MARKER_RING) {
			selectedRing = new Coordinates(letter, number);
			if (state == State.BLACK_MARKER_RING) {
			    selectedColor = org.openxum.common.Color.BLACK;
			} else {
			    selectedColor = org.openxum.common.Color.WHITE;
			}
		    }
		} else if (activity.getPhase() == Phase.MOVE_RING
			|| activity.getPhase() == Phase.MOVE_RING_OTHER) {
		    if (selectedRing.isValid()) {
			Coordinates coordinates = new Coordinates(letter,
				number);
			try {
			    if (activity
				    .verifyMoving(selectedRing, coordinates)) {
				selectedCoordinates = coordinates;
				if (activity.getPhase() == Phase.MOVE_RING) {
				    activity.moveRing();
				} else {
				    activity.moveRingOther();
				}
				selectedRing = new Coordinates('X', 0);
			    }
			} catch (OpenxumException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		    }
		} else if (activity.getPhase() == Phase.REMOVE_ROW
			|| activity.getPhase() == Phase.REMOVE_ROW_OTHER) {
		    selectedCoordinates = new Coordinates(letter, number);
		    try {
			if (activity.getPhase() == Phase.REMOVE_ROW) {
			    activity.removeRow();
			} else {
			    activity.removeRowOther();
			}
		    } catch (OpenxumException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    }
		    selectedRow.clear();
		} else if (activity.getPhase() == Phase.REMOVE_RING
			|| activity.getPhase() == Phase.REMOVE_RING_OTHER) {
		    if ((activity.getIntersectionState(letter, number) == State.BLACK_RING && activity
			    .getCurrentColor() == org.openxum.common.Color.BLACK)
			    || (activity.getIntersectionState(letter, number) == State.WHITE_RING && activity
				    .getCurrentColor() == org.openxum.common.Color.WHITE)) {
			selectedCoordinates = new Coordinates(letter, number);
			try {
			    if (activity.getPhase() == Phase.REMOVE_RING) {
				activity.removeRing();
			    } else {
				activity.removeRingOther();
			    }
			} catch (OpenxumException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		    }
		}
		invalidate();
	    }
	} else if (action == MotionEvent.ACTION_MOVE) {
	    if (activity.getPhase() == Phase.REMOVE_ROW
		    || activity.getPhase() == Phase.REMOVE_ROW_OTHER) {
		int xTouch = (int) event.getX();
		int yTouch = (int) event.getY();
		char letter = computeLetter(xTouch, yTouch);
		int number = computeNumber(xTouch, yTouch);

		if (letter != 'X'
			&& number != -1
			&& !selectedRow
				.contains(new Coordinates(letter, number))) {
		    selectedRow.add(new Coordinates(letter, number));
		    invalidate();
		}
	    }
	}
	return true;
    }

    private Coordinates selectedCoordinates;
    private Coordinates selectedRing;
    private org.openxum.common.Color selectedColor;
    private Row selectedRow;
}