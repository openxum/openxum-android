package org.openxum.yinsh.gui;

import java.util.List;

import org.openxum.R;
import org.openxum.common.Color;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class YinshPlayerAdapter extends BaseAdapter {

    public YinshPlayerAdapter(Context context, List<YinshRemotePlayer> players) {
	inflater = LayoutInflater.from(context);
	this.players = players;
    }

    @Override
    public int getCount() {
	return players.size();
    }

    @Override
    public Object getItem(int index) {
	return players.get(index);
    }

    @Override
    public long getItemId(int index) {
	return index;
    }

    private class ViewHolder {
	TextView nameTextView;
	TextView colorTextView;
    }

    @Override
    public View getView(int index, View convertView, ViewGroup parent) {
	ViewHolder holder;

	if (convertView == null) {
	    holder = new ViewHolder();
	    convertView = inflater.inflate(R.layout.yinsh_remote_player, null);
	    holder.nameTextView = (TextView) convertView
		    .findViewById(R.id.yinshPlayerName);
	    holder.colorTextView = (TextView) convertView
		    .findViewById(R.id.yinshPlayerColor);
	    convertView.setTag(holder);
	} else {
	    holder = (ViewHolder) convertView.getTag();
	}
	holder.nameTextView.setText(players.get(index).getName());
	holder.colorTextView
		.setText("Yinsh - " + (players.get(index).getColor() == Color.BLACK ? "Black"
			: "White"));
	return convertView;
    }

    private List<YinshRemotePlayer> players;
    private LayoutInflater inflater;
}
