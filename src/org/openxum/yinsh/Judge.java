/**
 * @file org/openxum/yinsh/Judge.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.yinsh;

import java.util.ListIterator;
import org.openxum.common.Color;
import org.openxum.common.InvalidMovingException;
import org.openxum.common.InvalidWinnerException;
import org.openxum.common.InvalidColorException;
import org.openxum.yinsh.game.Board;
import org.openxum.yinsh.game.Coordinates;
import org.openxum.yinsh.game.GameType;
import org.openxum.yinsh.game.Intersections;
import org.openxum.yinsh.game.InvalidPuttingException;
import org.openxum.yinsh.game.InvalidRemovingRingException;
import org.openxum.yinsh.game.InvalidRemovingRowException;
import org.openxum.yinsh.game.Row;
import org.openxum.yinsh.game.Rows;
import org.openxum.yinsh.game.State;
import org.openxum.yinsh.game.Board.Phase;
import org.openxum.yinsh.player.Player;

public class Judge {

    public Judge(GameType type, Color color) {
	this.board = new Board(type, color);
	this.firstPlaying = true;
    }

    public String displayBoard() {
	return this.board.toString();
    }

    public boolean existIntersection(char letter, int number) {
	return this.board.existIntersection(letter, number);
    }

    public Board getBoard() {
	return this.board;
    }

    public Color getCurrentColor() {
	return this.board.getCurrentColor();
    }

    public State getIntersectionState(char letter, int number) {
	return this.board.getIntersectionState(letter, number);
    }

    public Intersections getIntersections() {
	return this.board.getIntersections();
    }

    public Color getNextColor() {
	return this.board.getCurrentColor() == Color.BLACK ? Color.WHITE
		: Color.BLACK;
    }

    public int getRemovedRingNumber(Color color) {
	return this.board.getRemovedRingNumber(color);
    }

    public void init(Player first_player, Player second_player)
	    throws InvalidPuttingException {
	if (firstPlaying) {
	    putRing(first_player, second_player);
	} else {
	    putRing(second_player, first_player);
	}
	firstPlaying = !firstPlaying;
    }

    public boolean isFinished() {
	return this.board.isFinished();
    }

    public void play(Player first_player, Player second_player)
	    throws InvalidMovingException, InvalidPuttingException,
	    InvalidRemovingRowException, InvalidRemovingRingException,
	    InvalidColorException, InvalidWinnerException {

	if (board.getCurrentColor() == first_player.getColor()) {
	    if (board.getPhase() == Board.Phase.PUT_RING) {
		putRing(first_player, second_player);
	    } else if (board.getPhase() == Board.Phase.REMOVE_ROWS_AFTER
		    || board.getPhase() == Board.Phase.REMOVE_ROWS_BEFORE) {
		removeRows(first_player, second_player);
	    } else if (board.getPhase() == Board.Phase.PUT_MARKER) {
		origin = putMarker(first_player, second_player);
	    } else if (board.getPhase() == Board.Phase.MOVE_RING) {
		moveRing(first_player, second_player, origin);
	    }
	} else {
	    if (board.getPhase() == Board.Phase.PUT_RING) {
		putRing(second_player, first_player);
	    } else if (board.getPhase() == Board.Phase.REMOVE_ROWS_AFTER
		    || board.getPhase() == Board.Phase.REMOVE_ROWS_BEFORE) {
		removeRows(second_player, first_player);
	    } else if (board.getPhase() == Board.Phase.PUT_MARKER) {
		origin = putMarker(second_player, first_player);
	    } else if (board.getPhase() == Board.Phase.MOVE_RING) {
		moveRing(second_player, first_player, origin);
	    }
	}
	firstPlaying = !firstPlaying;
    }

    public void setBoard(Board board) {
	this.board = new Board(board);
    }

    void setOrigin(Coordinates origin) {
	this.origin = origin;
    }

    public Color winnerIs() throws InvalidWinnerException {
	return this.board.winnerIs();
    }

    /* - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - - */

    private void moveRing(Player current_player, Player other_player,
	    Coordinates origin) throws InvalidMovingException {
	Coordinates coordinates = current_player.moveRing(origin);

	// System.out
	// .println("move "
	// + (current_player.getColor() == Color.BLACK ? "black"
	// : "white") + " ring to "
	// + coordinates.getLetter() + coordinates.getNumber());

	other_player.moveRing(origin, coordinates, current_player.getColor());
	this.board.moveRing(origin, coordinates);
    }

    private Coordinates putMarker(Player current_player, Player other_player)
	    throws InvalidMovingException, InvalidPuttingException,
	    InvalidRemovingRowException, InvalidRemovingRingException,
	    InvalidColorException, InvalidWinnerException {
	Color player_color = current_player.getColor();
	Coordinates coordinates = current_player.putMarker();

	// System.out.println("put "
	// + (player_color == Color.BLACK ? "black" : "white")
	// + " marker at " + coordinates.getLetter()
	// + coordinates.getNumber());

	other_player.putMarker(coordinates, player_color);
	this.board.putMarker(coordinates, player_color);
	return coordinates;
    }

    private void putRing(Player current_player, Player other_player)
	    throws InvalidPuttingException {
	Color player_color = current_player.getColor();
	Coordinates coordinates = current_player.putRing();

	other_player.putRing(coordinates, player_color);
	this.board.putRing(coordinates, player_color);
    }

    private void removeRows(Player current_player, Player other_player)
	    throws InvalidMovingException, InvalidPuttingException,
	    InvalidRemovingRowException, InvalidRemovingRingException,
	    InvalidColorException {
	Color player_color = current_player.getColor();
	Rows rows = new Rows();

	if (current_player.removeRows(rows)) {
	    // System.out.print("remove "
	    // + (player_color == Color.BLACK ? "black" : "white"));
	    // if (rows.size() == 1) {
	    // System.out.println(" row: "
	    // + ((Row) rows.firstElement()).toString());
	    // } else {
	    // System.out.print(" rows:");
	    // for (ListIterator<Row> it = rows.listIterator(); it.hasNext();) {
	    // System.out.print(" " + ((Row) it.next()).toString());
	    // }
	    // System.out.println("");
	    // }

	    other_player.removeRows(rows, player_color);
	    this.board.removeRows(rows, player_color);

	    Coordinates coordinates = current_player.removeRing();

	    // System.out.println("remove "
	    // + (player_color == Color.BLACK ? "black" : "white")
	    // + " ring at " + coordinates.getLetter()
	    // + coordinates.getNumber());

	    other_player.removeRing(coordinates, player_color);
	    this.board.removeRing(coordinates, player_color);
	} else {
	    other_player.removeNoRow(player_color);
	    this.board.removeNoRow();
	}
    }

    private Board board;
    private boolean firstPlaying;
    private Coordinates origin;
}