package org.openxum.yinsh.player;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.openxum.GameActivity;
import org.openxum.common.Color;
import org.openxum.common.InvalidColorException;
import org.openxum.common.InvalidMovingException;
import org.openxum.yinsh.game.Coordinates;
import org.openxum.yinsh.game.GameType;
import org.openxum.yinsh.game.InvalidPuttingException;
import org.openxum.yinsh.game.InvalidRemovingRingException;
import org.openxum.yinsh.game.InvalidRemovingRowException;
import org.openxum.yinsh.game.Rows;
import org.openxum.yinsh.gui.Phase;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class RemotePlayer extends Player {

    public RemotePlayer(GameActivity activity, GameType type, Color color,
	    Color mycolor) {
	super(mycolor);
	this.activity = activity;

	SharedPreferences prefs = PreferenceManager
		.getDefaultSharedPreferences(activity);

	login = prefs.getString("login", "");
	password = prefs.getString("password", "");

	client = new DefaultHttpClient();
	connect(activity, login, password);
	index = 0;
    }

    public boolean isRemote() {
	return true;
    }

    private void connect(GameActivity activity, String login, String password) {
	HttpPost httppost = new HttpPost(OPENXUM_URI);
	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

	nameValuePairs.add(new BasicNameValuePair("action", "connect"));
	nameValuePairs.add(new BasicNameValuePair("login", login));
	nameValuePairs.add(new BasicNameValuePair("password", password));
	try {
	    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	    if (!parseStatus(client.execute(httppost))) {
		Toast.makeText(activity.getApplicationContext(),
			"Error to connect to game server", Toast.LENGTH_LONG)
			.show();
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    private boolean parseStatus(HttpResponse response) {
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

	try {
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    Document doc = builder.parse(response.getEntity().getContent());
	    Element root = doc.getDocumentElement();

	    if (root.getNodeName().compareTo("status") == 0) {
		return (root.getAttributes().getNamedItem("value")
			.getNodeValue().compareTo("OK") == 0);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return false;
    }

    private void sendTurn(String infos, Phase phase) {
	HttpGet request = new HttpGet(OPENXUM_URI
		+ "?game=yinsh&action=new_turn&id_game="
		+ GameActivity.currentGameID + "&color=" + color + "&type="
		+ phase + "&infos=" + infos);

	try {
	    client.execute(request);
	    ++index;
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    @Override
    public void finish() {
    }

    @Override
    public void moveRing(Coordinates origin, Coordinates destination,
	    Color color) throws InvalidMovingException {
	sendTurn(origin.encode() + ";" + destination.encode(), Phase.MOVE_RING);
    }

    @Override
    public void putMarker(Coordinates coordinates, Color color)
	    throws InvalidMovingException, InvalidPuttingException {
	sendTurn(coordinates.encode(), Phase.PUT_MARKER);
    }

    @Override
    public void putRing(Coordinates coordinates, Color color)
	    throws InvalidPuttingException {
	sendTurn(coordinates.encode(), Phase.PUT_RING);
    }

    @Override
    public void removeRing(Coordinates coordinates, Color color)
	    throws InvalidRemovingRingException, InvalidColorException {
	sendTurn(coordinates.encode(), Phase.REMOVE_RING);
    }

    @Override
    public void removeRows(Rows rows, Color color)
	    throws InvalidRemovingRowException {
	sendTurn(rows.encode(), Phase.REMOVE_ROW);
    }

    @Override
    public Coordinates moveRing(Coordinates coordinates)
	    throws InvalidMovingException {
	return this.coordinates;
    }

    @Override
    public Coordinates putMarker() throws InvalidMovingException,
	    InvalidPuttingException {
	return coordinates;
    }

    @Override
    public Coordinates putRing() throws InvalidPuttingException {
	return coordinates;
    }

    @Override
    public Coordinates removeRing() throws InvalidRemovingRingException,
	    InvalidColorException {
	return coordinates;
    }

    @Override
    public boolean removeRows(Rows rows) throws InvalidRemovingRowException {
	rows.clear();
	rows.addAll(this.rows);
	return true;
    }

    public void waitMoveRing() {
	new MoveRingTask().execute();
    }

    public void waitPutMarker() {
	new PutMarkerTask().execute();
    }

    public void waitPutRing() {
	new PutRingTask().execute();
    }

    public void waitRemoveRing() {
	new RemoveRingTask().execute();
    }

    public void waitRemoveRow() {
	new RemoveRowTask().execute();
    }

    private abstract class RemotePlayerTask extends AsyncTask<Void, Void, Void> {
	public RemotePlayerTask() {
	}

	protected boolean parseResponse(HttpResponse response) {
	    DocumentBuilderFactory factory = DocumentBuilderFactory
		    .newInstance();

	    try {
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(response.getEntity().getContent());
		Element root = doc.getDocumentElement();

		if (root.getNodeName().compareTo("turn") == 0) {
		    type = root.getAttributes().getNamedItem("type")
			    .getNodeValue();
		    infos = root.getAttributes().getNamedItem("infos")
			    .getNodeValue();
		    return true;
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	    return false;
	}

	@Override
	protected synchronized Void doInBackground(Void... params) {
	    boolean ok = false;
	    boolean stop = false;

	    ++index;
	    while (!ok && !stop) {
		try {
		    HttpGet request = new HttpGet(OPENXUM_URI
			    + "?game=yinsh&action=get_turn&id_game="
			    + GameActivity.currentGameID + "&index=" + index);
		    HttpResponse response = client.execute(request); 
		    
		    ok = parseResponse(response);
		    response.getEntity().consumeContent();
		    if (!ok) {
			wait(3000);
		    }
		} catch (Exception e) {
		    stop = true;
		    e.printStackTrace();
		}
	    }
	    if (stop) {
		coordinates = null;
	    } else {
		parseInfos();
	    }
	    return null;
	}

	protected abstract void parseInfos();

	protected String type;
	protected String infos;
    }

    private class MoveRingTask extends RemotePlayerTask {
	public MoveRingTask() {
	}

	protected void parseInfos() {
	    String str[] = infos.split(";");

	    coordinates = new Coordinates();
	    coordinates.decode(str[1]);
	}

	protected void onPostExecute(Void result) {
	    if (coordinates != null) {
		try {
		    activity.moveRingOther();
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	}
    }

    private class PutMarkerTask extends RemotePlayerTask {
	public PutMarkerTask() {
	}

	protected void parseInfos() {
	    coordinates = new Coordinates();
	    coordinates.decode(infos);
	}

	protected void onPostExecute(Void result) {
	    if (coordinates != null) {
		try {
		    activity.putMarkerOther();
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	}
    }

    private class PutRingTask extends RemotePlayerTask {
	public PutRingTask() {
	}

	protected void parseInfos() {
	    coordinates = new Coordinates();
	    coordinates.decode(infos);
	}

	protected void onPostExecute(Void result) {
	    if (coordinates != null) {
		try {
		    activity.putRingOther();
		} catch (InvalidPuttingException e) {
		    e.printStackTrace();
		}
	    }
	}
    }

    private class RemoveRingTask extends RemotePlayerTask {
	public RemoveRingTask() {
	}

	protected void parseInfos() {
	    coordinates = new Coordinates();
	    coordinates.decode(infos);
	}

	protected void onPostExecute(Void result) {
	    if (coordinates != null) {
		try {
		    activity.removeRingOther();
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	}
    }

    private class RemoveRowTask extends RemotePlayerTask {
	public RemoveRowTask() {
	}

	protected void parseInfos() {
	    rows = new Rows();
	    rows.decode(infos);
	}

	protected void onPostExecute(Void result) {
	    if (rows != null) {
		try {
		    activity.removeRowOther();
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	}
    }

    private GameActivity activity;
    private DefaultHttpClient client;
    private String login;
    private String password;
    private int index;
    private Coordinates coordinates;
    private Rows rows;
    private final static String OPENXUM_URI = "http://www.openxum.org/remote/index.php";
    @Override
    public void removeNoRow(Color color) {
	// TODO Auto-generated method stub
	
    }
}
