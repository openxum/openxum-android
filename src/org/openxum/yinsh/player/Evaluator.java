/**
 * @file org/openxum/yinsh/player/Evaluator.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.yinsh.player;

import java.util.ListIterator;
import java.util.Random;

import org.openxum.common.Color;
import org.openxum.common.InvalidColorException;
import org.openxum.common.InvalidMovingException;
import org.openxum.common.InvalidWinnerException;
import org.openxum.yinsh.Judge;
import org.openxum.yinsh.game.Board;
import org.openxum.yinsh.game.Coordinates;
import org.openxum.yinsh.game.CoordinatesList;
import org.openxum.yinsh.game.Intersection;
import org.openxum.yinsh.game.InvalidPuttingException;
import org.openxum.yinsh.game.InvalidRemovingRingException;
import org.openxum.yinsh.game.InvalidRemovingRowException;
import org.openxum.yinsh.game.PossibleMovingList;
import org.openxum.yinsh.game.State;

public class Evaluator {
    public Evaluator(Color color, Board board) {
	this.color = color;
	this.board = board;
    }

    Coordinates getMarkerCoordinates() {
	return this.markerCoordinates;
    }

    Coordinates getRingCoordinates() {
	return this.ringCoordinates;
    }

    public void putMarkerEvaluate() throws InvalidPuttingException,
	    InvalidMovingException, InvalidRemovingRowException,
	    InvalidRemovingRingException, InvalidColorException,
	    InvalidWinnerException {
	CoordinatesList list = board.getPlacedRingCoordinates(color);
	double max = -1000;

	ListIterator<Coordinates> it = list.listIterator();

	while (it.hasNext()) {
	    Coordinates c = (Coordinates) it.next();
	    double value;
	    Board board = new Board(this.board);

	    board.putMarker(c, color);
	    value = movingRingEvaluate(board, c);
	    if (value > max) {
		markerCoordinates = c;
		ringCoordinates = ringCoordinates2;
		max = value;
	    }
	}
    }

    public void putRingEvaluate() {
	CoordinatesList list = board
		.getPlacedRingCoordinates(color == Color.BLACK ? Color.WHITE
			: Color.BLACK);
	Random r = new Random();

	if (list.size() == 0) {
	    CoordinatesList l = this.board.getFreeIntersections();

	    ringCoordinates = (Coordinates) l.get(r.nextInt(l.size()));
	} else {
	    do {
		Coordinates coordinates = list.elementAt(r.nextInt(list.size()));
		int delta_letter = r.nextInt() % 3 - 1;
		int delta_number = r.nextInt() % 3 - 1;

		ringCoordinates = new Coordinates(
			(char) (coordinates.getLetter() + delta_letter),
			coordinates.getNumber() + delta_number);
	    } while (!board.existIntersection(ringCoordinates.getLetter(),
		    ringCoordinates.getNumber())
		    || board.getIntersectionState(ringCoordinates.getLetter(),
			    ringCoordinates.getNumber()) != State.VACANT);
	}
    }

    private double evaluate(Board board) throws InvalidMovingException,
	    InvalidPuttingException, InvalidRemovingRowException,
	    InvalidRemovingRingException, InvalidColorException,
	    InvalidWinnerException {
	double value = 0;
	int sampleNumber = 10; // Settings::settings().getYinshLevelNumber() *
			       // 10;

	for (int i = 1; i <= sampleNumber; ++i) {
	    Judge judge = new Judge(board.getType(), board.getCurrentColor());
	    RandomPlayer player_one = new RandomPlayer(board.getType(),
		    judge.getCurrentColor(), judge.getCurrentColor());
	    RandomPlayer player_two = new RandomPlayer(board.getType(),
		    judge.getCurrentColor(), judge.getNextColor());

	    judge.setBoard(board);
	    player_one.setBoard(board);
	    player_two.setBoard(board);
	    while (!judge.isFinished()) {
		judge.play(player_one, player_two);
	    }
	    value += judge.winnerIs() == color ? 100 : -1000;
	}
	return value / sampleNumber;
    }

    private double movingRingEvaluate(Board board, Coordinates origin)
	    throws InvalidMovingException, InvalidPuttingException,
	    InvalidRemovingRowException, InvalidRemovingRingException,
	    InvalidColorException, InvalidWinnerException {
	PossibleMovingList list = board.getPossibleMovingList(origin, color,
		true);
	double max = -1000;

	ListIterator<Intersection> it = list.listIterator();

	while (it.hasNext()) {
	    Intersection i = (Intersection) it.next();

	    double value;
	    Board new_board = new Board(board);

	    new_board.moveRing(origin, i.getCoordinates());
	    value = evaluate(new_board);
	    if (value > max) {
		ringCoordinates2 = i.getCoordinates();
		max = value;
	    }
	}
	return max;
    }

    private Color color;
    private Board board;
    private Coordinates markerCoordinates;
    private Coordinates ringCoordinates;
    private Coordinates ringCoordinates2;
}
