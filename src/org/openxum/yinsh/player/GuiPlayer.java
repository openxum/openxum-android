/**
 * @file org/openxum/yinsh/player/GuiPlayer.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.yinsh.player;

import java.util.ListIterator;

import org.openxum.common.Color;
import org.openxum.common.InvalidColorException;
import org.openxum.common.InvalidMovingException;
import org.openxum.yinsh.game.Coordinates;
import org.openxum.yinsh.game.GameType;
import org.openxum.yinsh.game.InvalidPuttingException;
import org.openxum.yinsh.game.InvalidRemovingRingException;
import org.openxum.yinsh.game.InvalidRemovingRowException;
import org.openxum.yinsh.game.Row;
import org.openxum.yinsh.game.Rows;
import org.openxum.yinsh.game.SeparatedRows;

public class GuiPlayer extends LocalPlayer {

    public GuiPlayer(org.openxum.yinsh.gui.BoardView view, GameType type,
	    Color color, Color mycolor) {
	super(type, color, mycolor);
	this.view = view;
    }

    @Override
    public boolean isGUI() {
	return true;
    }

    public Coordinates moveRing(Coordinates origin)
	    throws InvalidMovingException {
	this.board.moveRing(origin, view.getSelectedCoordinates());
	return view.getSelectedCoordinates();
    }

    public Coordinates putMarker() throws InvalidMovingException,
	    InvalidPuttingException {
	this.board.putMarker(view.getSelectedCoordinates(), color);
	return view.getSelectedCoordinates();
    }

    public Coordinates putRing() throws InvalidPuttingException {
	this.board.putRing(view.getSelectedCoordinates(), color);
	return view.getSelectedCoordinates();
    }

    public Coordinates removeRing() throws InvalidRemovingRingException,
	    InvalidColorException {
	this.board.removeRing(view.getSelectedCoordinates(), color);
	return view.getSelectedCoordinates();
    }

    public boolean removeRows(Rows rows) throws InvalidRemovingRowException {
	SeparatedRows srows = this.board.getRows(color);
	ListIterator<Rows> its = srows.listIterator();
	boolean found = false;

	while (!found && its.hasNext()) {
	    ListIterator<Row> itr = its.next().listIterator();

	    while (!found && itr.hasNext()) {
		Row row = itr.next();

		if (row.contains(view.getSelectedCoordinates())) {
		    found = true;
		    if (row.size() == 5) {
			rows.add(row);
		    } else if (view.getSelectedRow().size() == 5) {
			rows.add(view.getSelectedRow());
		    }
		}
	    }
	}  
	if (found) {
	    this.board.removeRows(rows, color);
	} else {
	    throw new InvalidRemovingRowException();
	}
	return found;
    }

    private org.openxum.yinsh.gui.BoardView view;
}
