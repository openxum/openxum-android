/**
 * @file org/openxum/yinsh/player/LocalPlayer.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.yinsh.player;

import org.openxum.common.Color;
import org.openxum.common.InvalidMovingException;
import org.openxum.common.InvalidColorException;
import org.openxum.yinsh.game.Board;
import org.openxum.yinsh.game.Coordinates;
import org.openxum.yinsh.game.GameType;
import org.openxum.yinsh.game.InvalidPuttingException;
import org.openxum.yinsh.game.InvalidRemovingRingException;
import org.openxum.yinsh.game.InvalidRemovingRowException;
import org.openxum.yinsh.game.Rows;

public abstract class LocalPlayer extends Player {

    public LocalPlayer(GameType type, Color color, Color mycolor) {
	super(mycolor);
	this.board = new Board(type, color);
    }

    void setBoard(Board board) {
	this.board = new Board(board);
    }

    public void moveRing(Coordinates origin, Coordinates destination,
	    Color color) throws InvalidMovingException {
	this.board.moveRing(origin, destination);
    }

    public void putMarker(Coordinates coordinates, Color color)
	    throws InvalidMovingException, InvalidPuttingException {
	this.board.putMarker(coordinates, color);
    }

    public void putRing(Coordinates coordinates, Color color)
	    throws InvalidPuttingException {
	this.board.putRing(coordinates, color);
    }

    public void removeRing(Coordinates coordinates, Color color)
	    throws InvalidRemovingRingException, InvalidColorException {
	this.board.removeRing(coordinates, color);
    }

    public void removeRows(Rows rows, Color color)
	    throws InvalidRemovingRowException {
	this.board.removeRows(rows, color);
    }

    public void removeNoRow(Color color) {
	this.board.removeNoRow();
    }

    protected Board board;
}