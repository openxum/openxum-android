/**
 * @file org/openxum/yinsh/player/Player.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.yinsh.player;

import org.openxum.common.Color;
import org.openxum.common.InvalidMovingException;
import org.openxum.common.InvalidColorException;
import org.openxum.common.InvalidWinnerException;
import org.openxum.yinsh.game.Coordinates;
import org.openxum.yinsh.game.InvalidPuttingException;
import org.openxum.yinsh.game.InvalidRemovingRingException;
import org.openxum.yinsh.game.InvalidRemovingRowException;
import org.openxum.yinsh.game.Rows;

public abstract class Player {

    public Player(Color mycolor) {
	this.color = mycolor;
    }

    public void finish() {
    }

    public Color getColor() {
	return this.color;
    }

    public boolean isGUI() {
	return false;
    }

    public boolean isRemote() {
	return false;
    }

    public abstract void moveRing(Coordinates origin, Coordinates destination,
	    Color color) throws InvalidMovingException;

    public abstract void putMarker(Coordinates coordinates, Color color)
	    throws InvalidMovingException, InvalidPuttingException;

    public abstract void putRing(Coordinates coordinates, Color color)
	    throws InvalidPuttingException;

    public abstract void removeRing(Coordinates coordinates, Color color)
	    throws InvalidRemovingRingException, InvalidColorException;

    public abstract void removeRows(Rows rows, Color color)
	    throws InvalidRemovingRowException;

    public abstract void removeNoRow(Color color);

    public abstract Coordinates moveRing(Coordinates coordinates)
	    throws InvalidMovingException;

    public abstract Coordinates putMarker() throws InvalidMovingException,
	    InvalidPuttingException, InvalidRemovingRowException,
	    InvalidRemovingRingException, InvalidColorException,
	    InvalidWinnerException;

    public abstract Coordinates putRing() throws InvalidPuttingException;

    public abstract Coordinates removeRing()
	    throws InvalidRemovingRingException, InvalidColorException;

    public abstract boolean removeRows(Rows rows)
	    throws InvalidRemovingRowException;

    protected void setColor(Color color) {
	this.color = color;
    }

    protected Color color;
}