/**
 * @file org/openxum/yinsh/player/RandomPlayer.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.yinsh.player;

import java.util.ListIterator;
import java.util.Random;
import org.openxum.common.Color;
import org.openxum.common.InvalidColorException;
import org.openxum.common.InvalidMovingException;
import org.openxum.common.InvalidWinnerException;
import org.openxum.yinsh.game.Coordinates;
import org.openxum.yinsh.game.CoordinatesList;
import org.openxum.yinsh.game.GameType;
import org.openxum.yinsh.game.Intersection;
import org.openxum.yinsh.game.InvalidPuttingException;
import org.openxum.yinsh.game.InvalidRemovingRingException;
import org.openxum.yinsh.game.InvalidRemovingRowException;
import org.openxum.yinsh.game.PossibleMovingList;
import org.openxum.yinsh.game.Row;
import org.openxum.yinsh.game.Rows;
import org.openxum.yinsh.game.SeparatedRows;

public class RandomPlayer extends LocalPlayer {

    public RandomPlayer(GameType type, Color color, Color mycolor) {
	super(type, color, mycolor);
    }

    public Coordinates moveRing(Coordinates origin)
	    throws InvalidMovingException {
	PossibleMovingList list = this.board.getPossibleMovingList(origin,
		this.color, true);

	if (list.size() > 0) {
	    Random r = new Random();
	    int moving_index = r.nextInt(list.size());
	    Intersection intersection = (Intersection) list.get(moving_index);

	    this.board.moveRing(origin, intersection.getCoordinates());
	    return new Coordinates(intersection.getLetter(),
		    intersection.getNumber());
	}
	throw new InvalidMovingException("yinsh: no position for ring moving");
    }

    public Coordinates putMarker() throws InvalidMovingException,
	    InvalidPuttingException, InvalidRemovingRowException,
	    InvalidRemovingRingException, InvalidColorException,
	    InvalidWinnerException {
	Random r = new Random();
	boolean ok = false;
	Coordinates ring_coordinates = null;

	while (!ok) {
	    int ring_index = r.nextInt(this.board.getPlacedRingCoordinates(
		    this.color).size());
	    ring_coordinates = (Coordinates) this.board
		    .getPlacedRingCoordinates(this.color).get(ring_index);
	    ok = this.board.getPossibleMovingList(ring_coordinates, this.color,
		    false).size() > 0;
	}

	this.board.putMarker(ring_coordinates, this.color);
	return ring_coordinates;
    }

    public Coordinates putRing() throws InvalidPuttingException {
	Random r = new Random();
	CoordinatesList list = this.board.getFreeIntersections();
	int index = r.nextInt(list.size());
	Coordinates coordinates = (Coordinates) list.get(index);

	this.board.putRing(coordinates, this.color);
	return coordinates;
    }

    public Coordinates removeRing() throws InvalidRemovingRingException,
	    InvalidColorException {
	Random r = new Random();
	int ring_index = r.nextInt(this.board.getPlacedRingCoordinates(
		this.color).size());
	Coordinates ring_coordinates = (Coordinates) this.board
		.getPlacedRingCoordinates(this.color).get(ring_index);

	this.board.removeRing(ring_coordinates, this.color);
	return ring_coordinates;
    }

    public boolean removeRows(Rows rows) throws InvalidRemovingRowException {
	boolean found = false;
	SeparatedRows srows = this.board.getRows(this.color);

	if (srows.size() > 0) {
	    found = true;
	    if (srows.size() == 1) {
		rows.add(removeRow((Rows) srows.firstElement()));
	    } else {
		ListIterator<Rows> it = srows.listIterator();

		while (it.hasNext()) {
		    rows.add(removeRow((Rows) it.next()));
		}
	    }
	    this.board.removeRows(rows, this.color);
	} else {
	    this.board.removeNoRow();
	}
	return found;
    }

    /* - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - - */

    private Row removeRow(Rows rows) {
	Random r = new Random();
	Row selectedRow = null;
	Row removedRow = null;

	if (rows.size() == 1)
	    selectedRow = (Row) rows.firstElement();
	else {
	    selectedRow = (Row) rows.get(r.nextInt(rows.size()));
	}
	if (selectedRow.size() == 5) {
	    removedRow = selectedRow;
	} else {
	    int index = r.nextInt(selectedRow.size() - 5);

	    removedRow = new Row((Coordinates) selectedRow.get(index),
		    (Coordinates) selectedRow.get(index + 4));
	}
	return removedRow;
    }
}