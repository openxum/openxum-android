/**
 * @file org/openxum/yinsh/player/MCTSPlayer.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.yinsh.player;

import org.openxum.common.Color;
import org.openxum.common.InvalidColorException;
import org.openxum.common.InvalidMovingException;
import org.openxum.common.InvalidWinnerException;
import org.openxum.yinsh.game.Coordinates;
import org.openxum.yinsh.game.GameType;
import org.openxum.yinsh.game.InvalidPuttingException;
import org.openxum.yinsh.game.InvalidRemovingRingException;
import org.openxum.yinsh.game.InvalidRemovingRowException;

public class MCTSPlayer extends RandomPlayer {

    public MCTSPlayer(GameType type, Color color, Color mycolor) {
	super(type, color, mycolor);
    }

    public Coordinates moveRing(Coordinates origin)
	    throws InvalidMovingException {
	this.board.moveRing(origin, ringCoordinates);
	return ringCoordinates;
    }

    public Coordinates putMarker() throws InvalidPuttingException,
	    InvalidMovingException, InvalidRemovingRowException,
	    InvalidRemovingRingException, InvalidColorException,
	    InvalidWinnerException {
	Evaluator e = new Evaluator(color, board);

	e.putMarkerEvaluate();
	this.markerCoordinates = e.getMarkerCoordinates();
	this.ringCoordinates = e.getRingCoordinates();
	this.board.putMarker(markerCoordinates, color);
	return markerCoordinates;
    }

    public Coordinates putRing() throws InvalidPuttingException {
	Evaluator e = new Evaluator(color, this.board);

	e.putRingEvaluate();
	this.board.putRing(e.getRingCoordinates(), color);
	return e.getRingCoordinates();
    }

    private Coordinates ringCoordinates;
    private Coordinates markerCoordinates;
}
