/**
 * @file org/openxum/yinsh/Main.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.yinsh;

import java.io.IOException;
import org.openxum.common.Color;
import org.openxum.common.OpenxumException;
import org.openxum.yinsh.game.GameType;
import org.openxum.yinsh.player.RandomPlayer;
import org.openxum.yinsh.player.Player;

public class Main {
    public static void main(String[] args) throws IOException {
	GameType type = GameType.BLITZ;
	Judge judge = new Judge(type, Color.BLACK);
	Player player_one = new RandomPlayer(type, judge.getCurrentColor(),
		judge.getCurrentColor());
	Player player_two = new RandomPlayer(type, judge.getCurrentColor(),
		judge.getNextColor());

	System.out.println("First player is: "
		+ (player_one.getColor() == Color.BLACK ? "black" : "white"));
	System.out.println("Second player is: "
		+ (player_two.getColor() == Color.BLACK ? "black" : "white"));
	try {
	    judge.init(player_one, player_two);
	} catch (OpenxumException e) {
	}
	System.out.println(judge.displayBoard());
	System.out.println("Board is initialized !");

	System.in.read();

	int i = 1;

	while (!judge.isFinished()) {
	    System.out.println("Turn n°" + i++);
	    try {
		judge.play(player_one, player_two);
	    } catch (OpenxumException e) {
		e.printStackTrace();
	    }

	    System.out.println(judge.displayBoard());

	    System.in.read();
	}
	try {
	    System.out.println("Winner is: "
		    + (judge.winnerIs() == Color.WHITE ? "WHITE" : judge
			    .winnerIs() == Color.BLACK ? "BLACK" : "NONE"));
	} catch (OpenxumException localOpenxumException2) {
	}
    }
}