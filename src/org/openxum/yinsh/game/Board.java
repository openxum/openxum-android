/**
 * @file org/openxum/yinsh/game/Board.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.yinsh.game;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map.Entry;
import java.util.Random;
import org.openxum.common.Color;
import org.openxum.common.InvalidColorException;
import org.openxum.common.InvalidMovingException;
import org.openxum.common.InvalidWinnerException;

public class Board {
    public enum Phase {
	PUT_RING, PUT_MARKER, MOVE_RING, REMOVE_ROWS_AFTER, REMOVE_RING_AFTER, REMOVE_ROWS_BEFORE, REMOVE_RING_BEFORE
    }

    static final char[] letter_matrix = { 'X', 'X', 'X', 'Z', 'X', 'E', 'X',
	    'G', 'X', 'X', 'X', 'X', 'X', 'X', 'Z', 'X', 'D', 'X', 'F', 'X',
	    'H', 'X', 'X', 'X', 'X', 'Z', 'X', 'C', 'X', 'E', 'X', 'G', 'X',
	    'I', 'X', 'X', 'X', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X',
	    'J', 'X', 'X', 'Z', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X',
	    'X', 'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
	    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'K', 'Z',
	    'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X', 'X', 'A',
	    'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'K', 'Z', 'X', 'B',
	    'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X', 'X', 'A', 'X', 'C',
	    'X', 'E', 'X', 'G', 'X', 'I', 'X', 'K', 'Z', 'X', 'B', 'X', 'D',
	    'X', 'F', 'X', 'H', 'X', 'J', 'X', 'X', 'A', 'X', 'C', 'X', 'E',
	    'X', 'G', 'X', 'I', 'X', 'K', 'X', 'X', 'B', 'X', 'D', 'X', 'F',
	    'X', 'H', 'X', 'J', 'X', 'X', 'Z', 'X', 'C', 'X', 'E', 'X', 'G',
	    'X', 'I', 'X', 'X', 'X', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H',
	    'X', 'J', 'X', 'X', 'X', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I',
	    'X', 'X', 'X', 'X', 'X', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'X',
	    'X', 'X', 'X', 'X', 'X', 'X', 'E', 'X', 'G', 'X', 'X', 'X', 'X' };

    static final char[] number_matrix = { 0, 0, 0, 0, 0, 10, 0, 11, 0, 0, 0, 0,
	    0, 0, 0, 0, 9, 0, 10, 0, 11, 0, 0, 0, 0, 0, 0, 8, 0, 9, 0, 10, 0,
	    11, 0, 0, 0, 0, 7, 0, 8, 0, 9, 0, 10, 0, 11, 0, 0, 0, 0, 7, 0, 8,
	    0, 9, 0, 10, 0, 0, 0, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0, 0, 5, 0, 6,
	    0, 7, 0, 8, 0, 9, 0, 10, 0, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 0, 4,
	    0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0,
	    0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 0, 3, 0, 4, 0, 5, 0, 6, 0,
	    7, 0, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 0, 2, 0, 3, 0, 4, 0,
	    5, 0, 6, 0, 0, 0, 0, 2, 0, 3, 0, 4, 0, 5, 0, 0, 0, 0, 1, 0, 2, 0,
	    3, 0, 4, 0, 5, 0, 0, 0, 0, 1, 0, 2, 0, 3, 0, 4, 0, 0, 0, 0, 0, 0,
	    1, 0, 2, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 0 };
    static final int INITIAL_MARKER_NUMBER = 51;
    public static final char[] BEGIN_LETTER = { 'B', 'A', 'A', 'A', 'A', 'B',
	    'B', 'C', 'D', 'E', 'G' };

    public static final char[] END_LETTER = { 'E', 'G', 'H', 'I', 'J', 'J',
	    'K', 'K', 'K', 'K', 'J' };

    public static final int[] BEGIN_NUMBER = { 2, 1, 1, 1, 1, 2, 2, 3, 4, 5, 7 };
    public static final int[] END_NUMBER = { 5, 7, 8, 9, 10, 10, 11, 11, 11,
	    11, 10 };
    public static final char[] BEGIN_DIAGONAL_LETTER = { 'B', 'A', 'A', 'A',
	    'A', 'B', 'B', 'C', 'D', 'E', 'G' };

    public static final char[] END_DIAGONAL_LETTER = { 'E', 'G', 'H', 'I', 'J',
	    'J', 'K', 'K', 'K', 'K', 'J' };

    public static final int[] BEGIN_DIAGONAL_NUMBER = { 7, 5, 4, 3, 2, 2, 1, 1,
	    1, 1, 2 };

    public static final int[] END_DIAGONAL_NUMBER = { 10, 11, 11, 11, 11, 10,
	    10, 9, 8, 7, 5 };

    public Board() {
	Random r = new Random();

	this.type = GameType.REGULAR;
	if (r.nextBoolean())
	    this.currentColor = Color.BLACK;
	else {
	    this.currentColor = Color.WHITE;
	}
	this.removedBlackRingNumber = 0;
	this.removedWhiteRingNumber = 0;
	this.markerNumber = INITIAL_MARKER_NUMBER;

	this.intersections = new Intersections();
	this.placedWhiteRingCoordinates = new CoordinatesList();
	this.placedBlackRingCoordinates = new CoordinatesList();
	for (char l = 'A'; l < 'L'; l++)
	    for (int n = BEGIN_NUMBER[(l - 'A')]; n <= END_NUMBER[(l - 'A')]; n++) {
		Coordinates coordinates = new Coordinates(l, n);

		this.intersections.put(coordinates, new Intersection(
			coordinates));
	    }
	phase = Phase.PUT_RING;
    }

    public Board(GameType type) {
	Random r = new Random();

	this.type = type;
	if (r.nextBoolean())
	    this.currentColor = Color.BLACK;
	else {
	    this.currentColor = Color.WHITE;
	}
	this.removedBlackRingNumber = 0;
	this.removedWhiteRingNumber = 0;
	this.markerNumber = INITIAL_MARKER_NUMBER;

	this.intersections = new Intersections();
	this.placedWhiteRingCoordinates = new CoordinatesList();
	this.placedBlackRingCoordinates = new CoordinatesList();
	for (char l = 'A'; l < 'L'; l++)
	    for (int n = BEGIN_NUMBER[(l - 'A')]; n <= END_NUMBER[(l - 'A')]; n++) {
		Coordinates coordinates = new Coordinates(l, n);

		this.intersections.put(coordinates, new Intersection(
			coordinates));
	    }
	phase = Phase.PUT_RING;
    }

    public Board(GameType type, Color color) {
	this.type = type;
	this.currentColor = color;
	this.removedBlackRingNumber = 0;
	this.removedWhiteRingNumber = 0;
	this.markerNumber = INITIAL_MARKER_NUMBER;

	this.intersections = new Intersections();
	this.placedWhiteRingCoordinates = new CoordinatesList();
	this.placedBlackRingCoordinates = new CoordinatesList();
	for (char l = 'A'; l < 'L'; l++)
	    for (int n = BEGIN_NUMBER[(l - 'A')]; n <= END_NUMBER[(l - 'A')]; n++) {
		Coordinates coordinates = new Coordinates(l, n);

		this.intersections.put(coordinates, new Intersection(
			coordinates));
	    }
	phase = Phase.PUT_RING;
    }

    public Board(Board board) {
	this.type = board.type;
	this.currentColor = board.currentColor;
	this.removedBlackRingNumber = board.removedBlackRingNumber;
	this.removedWhiteRingNumber = board.removedWhiteRingNumber;
	this.markerNumber = board.markerNumber;
	this.phase = board.phase;

	this.intersections = new Intersections();
	this.placedWhiteRingCoordinates = new CoordinatesList(
		board.placedWhiteRingCoordinates);
	this.placedBlackRingCoordinates = new CoordinatesList(
		board.placedBlackRingCoordinates);
	for (char l = 'A'; l < 'L'; l++)
	    for (int n = BEGIN_NUMBER[(l - 'A')]; n <= END_NUMBER[(l - 'A')]; n++) {
		Coordinates coordinates = new Coordinates(l, n);

		this.intersections.put(coordinates,
			new Intersection(board.getIntersection(l, n)));
	    }
    }

    public boolean existIntersection(char letter, int number) {
	Coordinates coordinates = new Coordinates(letter, number);

	if (coordinates.isValid()) {
	    return this.intersections.containsKey(coordinates);
	}
	return false;
    }

    public int getAvailableMarkerNumber() {
	return this.markerNumber;
    }

    public Color getCurrentColor() {
	return this.currentColor;
    }

    public CoordinatesList getFreeIntersections() {
	CoordinatesList list = new CoordinatesList();
	Iterator<Entry<Coordinates, Intersection>> it = this.intersections
		.entrySet().iterator();

	while (it.hasNext()) {
	    Entry<Coordinates, Intersection> entry = it.next();

	    if (((Intersection) entry.getValue()).getState() == State.VACANT) {
		list.add((Coordinates) entry.getKey());
	    }
	}
	return list;
    }

    public CoordinatesList getPlacedRingCoordinates(Color color) {
	if (color == Color.BLACK) {
	    return this.placedBlackRingCoordinates;
	}
	return this.placedWhiteRingCoordinates;
    }

    public PossibleMovingList getPossibleMovingList(Coordinates origin,
	    Color color, boolean control) throws InvalidMovingException {
	PossibleMovingList list = new PossibleMovingList();

	if (!this.intersections.containsKey(origin)) {
	    throw new InvalidMovingException("yinsh: invalid coordinates");
	}

	Intersection intersection = (Intersection) this.intersections
		.get(origin);

	if ((control)
		&& ((intersection.getState() != State.BLACK_MARKER_RING) || (color != Color.BLACK))
		&& ((intersection.getState() != State.WHITE_MARKER_RING) || (color != Color.WHITE))) {
	    throw new InvalidMovingException("yinsh: invalid color");
	}

	{
	    this.pOk = true;
	    int n = intersection.getNumber() + 1;
	    this.pNoVacant = false;

	    while ((n <= END_NUMBER[(intersection.getLetter() - 'A')])
		    && (this.pOk)) {
		verifyIntersection(intersection.getLetter(), n);
		if (((this.pOk) && (!this.pNoVacant))
			|| ((!this.pOk) && (this.pNoVacant))) {
		    list.add(getIntersection(intersection.getLetter(), n));
		}
		n++;
	    }
	}

	{
	    this.pOk = true;
	    int n = intersection.getNumber() - 1;
	    this.pNoVacant = false;

	    while ((n >= BEGIN_NUMBER[(intersection.getLetter() - 'A')])
		    && (this.pOk)) {
		verifyIntersection(intersection.getLetter(), n);
		if (((this.pOk) && (!this.pNoVacant))
			|| ((!this.pOk) && (this.pNoVacant))) {
		    list.add(getIntersection(intersection.getLetter(), n));
		}
		n--;
	    }
	}
	{
	    this.pOk = true;
	    char l = (char) (intersection.getLetter() + '\001');
	    this.pNoVacant = false;

	    while ((l <= END_LETTER[(intersection.getNumber() - 1)])
		    && (this.pOk)) {
		verifyIntersection(l, intersection.getNumber());
		if (((this.pOk) && (!this.pNoVacant))
			|| ((!this.pOk) && (this.pNoVacant))) {
		    list.add(getIntersection(l, intersection.getNumber()));
		}
		l = (char) (l + '\001');
	    }
	}
	{
	    this.pOk = true;
	    char l = (char) (intersection.getLetter() - '\001');
	    this.pNoVacant = false;

	    while ((l >= BEGIN_LETTER[(intersection.getNumber() - 1)])
		    && (this.pOk)) {
		verifyIntersection(l, intersection.getNumber());
		if (((this.pOk) && (!this.pNoVacant))
			|| ((!this.pOk) && (this.pNoVacant))) {
		    list.add(getIntersection(l, intersection.getNumber()));
		}
		l = (char) (l - '\001');
	    }
	}
	{
	    this.pOk = true;
	    int n = intersection.getNumber() + 1;
	    char l = (char) (intersection.getLetter() + '\001');
	    this.pNoVacant = false;

	    while ((l - 'A' <= 10) && (n - 1 <= 10)
		    && (n <= END_NUMBER[(l - 'A')])
		    && (l <= END_LETTER[(n - 1)]) && (this.pOk)) {
		verifyIntersection(l, n);
		if (((this.pOk) && (!this.pNoVacant))
			|| ((!this.pOk) && (this.pNoVacant))) {
		    list.add(getIntersection(l, n));
		}
		l = (char) (l + '\001');
		n++;
	    }
	}
	{
	    this.pOk = true;
	    int n = intersection.getNumber() - 1;
	    char l = (char) (intersection.getLetter() - '\001');
	    this.pNoVacant = false;

	    while ((l - 'A' >= 0) && (n - 1 >= 0)
		    && (n >= BEGIN_NUMBER[(l - 'A')])
		    && (l >= BEGIN_LETTER[(n - 1)]) && (this.pOk)) {
		verifyIntersection(l, n);
		if (((this.pOk) && (!this.pNoVacant))
			|| ((!this.pOk) && (this.pNoVacant))) {
		    list.add(getIntersection(l, n));
		}
		l = (char) (l - '\001');
		n--;
	    }
	}
	return list;
    }

    public int getRemovedRingNumber(Color color) {
	return color == Color.BLACK ? this.removedBlackRingNumber
		: this.removedWhiteRingNumber;
    }

    public SeparatedRows getRows(Color color) {
	Rows rows = new Rows();
	State state = color == Color.BLACK ? State.BLACK_MARKER
		: State.WHITE_MARKER;

	for (int n = 1; n <= 11; n++) {
	    this.pStart = false;
	    Row row = new Row();

	    for (char l = BEGIN_LETTER[(n - 1)]; l <= END_LETTER[(n - 1)]; l = (char) (l + '\001')) {
		buildRow(l, n, state, rows, row);
	    }
	    if (row.size() >= 5) {
		rows.add(row);
	    }
	}

	for (char l = 'A'; l <= 'K'; l = (char) (l + '\001')) {
	    this.pStart = false;
	    Row row = new Row();

	    for (int n = BEGIN_NUMBER[(l - 'A')]; n <= END_NUMBER[(l - 'A')]; n++) {
		buildRow(l, n, state, rows, row);
	    }
	    if (row.size() >= 5) {
		rows.add(row);
	    }
	}

	for (int i = 0; i <= 10; i++) {
	    this.pStart = false;
	    Row row = new Row();
	    int n = BEGIN_DIAGONAL_NUMBER[i];
	    char l = BEGIN_DIAGONAL_LETTER[i];

	    while ((l <= END_DIAGONAL_LETTER[i])
		    && (n <= END_DIAGONAL_NUMBER[i])) {
		buildRow(l, n, state, rows, row);
		l = (char) (l + '\001');
		n++;
	    }
	    if (row.size() >= 5) {
		rows.add(row);
	    }
	}

	SeparatedRows srows = new SeparatedRows();

	if (rows.size() != 0) {
	    Rows list = new Rows();

	    list.add((Row) rows.lastElement());
	    srows.add(list);
	    rows.remove(rows.lastElement());
	    while (rows.size() != 0) {
		Row row = (Row) rows.lastElement();
		boolean found = false;
		ListIterator<Rows> it = srows.listIterator();

		while ((it.hasNext()) && (!found)) {
		    Rows r = (Rows) it.next();
		    ListIterator<Row> itr = r.listIterator();

		    while ((itr.hasNext()) && (!found)) {
			if (!row.isSeparated((Row) itr.next())) {
			    r.add(row);
			    found = true;
			}
		    }
		}
		if (!found) {
		    Rows l = new Rows();

		    l.add(row);
		    srows.add(l);
		}
		rows.remove(rows.lastElement());
	    }
	}
	return srows;
    }

    public State getIntersectionState(char letter, int number) {
	return getIntersection(letter, number).getState();
    }

    public Intersections getIntersections() {
	return this.intersections;
    }

    public Phase getPhase() {
	return phase;
    }

    public GameType getType() {
	return type;
    }

    public boolean isFinished() {
	if (this.type == GameType.BLITZ) {
	    return (this.removedBlackRingNumber == 1)
		    || (this.removedWhiteRingNumber == 1)
		    || (this.markerNumber == 0);
	}

	return (this.removedBlackRingNumber == 3)
		|| (this.removedWhiteRingNumber == 3)
		|| (this.markerNumber == 0);
    }

    public boolean isInitialized() {
	return (this.placedBlackRingCoordinates.size() == 5)
		&& (this.placedWhiteRingCoordinates.size() == 5);
    }

    public void moveRing(Coordinates origin, Coordinates destination)
	    throws InvalidMovingException {
	if (!this.intersections.containsKey(origin)) {
	    throw new InvalidMovingException(
		    "yinsh: invalid origin coordinates");
	}
	if (!this.intersections.containsKey(destination)) {
	    throw new InvalidMovingException(
		    "yinsh: invalid destination coordinates");
	}

	Intersection originIntersection = (Intersection) this.intersections
		.get(origin);
	Intersection destinationIntersection = (Intersection) this.intersections
		.get(destination);

	if (destinationIntersection.getState() != State.VACANT) {
	    throw new InvalidMovingException("yinsh: destination not vacant");
	}
	if (!verifyMoving(originIntersection, destinationIntersection)) {
	    throw new InvalidMovingException("yinsh: invalid moving");
	}
	try {
	    Color color = originIntersection.getColor();

	    originIntersection.removeRing();
	    destinationIntersection.putRing(color);
	    flip(originIntersection, destinationIntersection);

	    if (color == Color.BLACK) {
		removeBlackRing(origin);
		this.placedBlackRingCoordinates.add(destination);
	    } else {
		removeWhiteRing(origin);
		this.placedWhiteRingCoordinates.add(destination);
	    }
	    phase = Phase.REMOVE_ROWS_AFTER;
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public void putMarker(Coordinates coordinates, Color color)
	    throws InvalidPuttingException, InvalidMovingException {
	if (this.markerNumber > 0) {
	    if (this.intersections.containsKey(coordinates))
		((Intersection) this.intersections.get(coordinates))
			.putMarker(color);
	    else {
		throw new InvalidPuttingException("yinsh: invalid coordinates");
	    }
	    this.markerNumber -= 1;
	} else {
	    throw new InvalidPuttingException(
		    "yinsh: the number of markers is null");
	}
	phase = Phase.MOVE_RING;
    }

    public void putRing(Coordinates coordinates, Color color)
	    throws InvalidPuttingException {
	if (color != this.currentColor) {
	    throw new InvalidPuttingException("yinsh: invalid color");
	}
	if (((color == Color.BLACK) && (this.placedBlackRingCoordinates.size() == 5))
		|| ((color == Color.WHITE) && (this.placedWhiteRingCoordinates
			.size() == 5))) {
	    throw new InvalidPuttingException("yinsh: too many rings");
	}

	if (this.intersections.containsKey(coordinates))
	    ((Intersection) this.intersections.get(coordinates)).putRing(color);
	else {
	    throw new InvalidPuttingException("yinsh: invalid coordinates");
	}
	if (color == Color.WHITE)
	    this.placedWhiteRingCoordinates.add(coordinates);
	else {
	    this.placedBlackRingCoordinates.add(coordinates);
	}
	if (placedBlackRingCoordinates.size() == 5
		&& placedWhiteRingCoordinates.size() == 5) {
	    phase = Phase.REMOVE_ROWS_BEFORE;
	}
	changeColor();
    }

    public void removeNoRow() {
	if (phase == Phase.REMOVE_ROWS_AFTER) {
	    changeColor();
	    phase = Phase.REMOVE_ROWS_BEFORE;
	} else {
	    phase = Phase.PUT_MARKER;
	}
    }

    public void removeRows(Rows rows, Color color)
	    throws InvalidRemovingRowException {
	ListIterator<Row> it = rows.listIterator();

	while (it.hasNext()) {
	    Row row = (Row) it.next();
	    ListIterator<Coordinates> itr = row.listIterator();

	    if (row.size() != 5) {
		throw new InvalidRemovingRowException(
			"yinsh: invalid row length");
	    }
	    do {
		Coordinates c = (Coordinates) itr.next();

		removeMarker(c.getLetter(), c.getNumber());
	    } while (itr.hasNext());
	}
	if (phase == Phase.REMOVE_ROWS_AFTER) {
	    phase = Phase.REMOVE_RING_AFTER;
	} else {
	    phase = Phase.REMOVE_RING_BEFORE;
	}
    }

    public void removeRing(Coordinates coordinates, Color color)
	    throws InvalidRemovingRingException, InvalidColorException {
	if (this.intersections.containsKey(coordinates)) {
	    Intersection intersection = (Intersection) this.intersections
		    .get(coordinates);

	    if (intersection.getColor() == color) {
		intersection.removeRingBoard();
	    } else {
		throw new InvalidRemovingRingException("yinsh: invalid color");
	    }
	} else {
	    throw new InvalidRemovingRingException("yinsh: invalid coordinates");
	}
	if (color == Color.BLACK) {
	    removeBlackRing(coordinates);
	    this.removedBlackRingNumber += 1;
	} else {
	    removeWhiteRing(coordinates);
	    this.removedWhiteRingNumber += 1;
	}
	if (phase == Phase.REMOVE_RING_AFTER) {
	    phase = Phase.REMOVE_ROWS_BEFORE;
	    changeColor();
	} else {
	    phase = Phase.PUT_MARKER;
	}
    }

    public String toString() {
	String str = new String("                       10        11\n");
	int k = 9;

	for (int j = 0; j < 19; ++j) {
	    for (int i = 0; i < 12; ++i) {
		char l = letter_matrix[(i + 12 * j)];
		int n = number_matrix[(i + 12 * j)];

		if ((l != 'X') && (l != 'Z')) {
		    Coordinates coordinates = new Coordinates(l, n);

		    if (this.intersections.containsKey(coordinates))
			str = str
				+ ((Intersection) this.intersections
					.get(coordinates)).toString();
		} else if (l == 'Z') {
		    String s = new String("    ");

		    s = s + (char) ('0' + k);
		    str = str + s;
		    k--;
		} else {
		    str = str + "     ";
		}
	    }
	    str = str + "\n";
	}
	str = str
		+ "      A    B    C    D    E    F    G    H    I    J    K\n";
	return str;
    }

    public boolean verifyMoving(Coordinates origin, Coordinates destination)
	    throws InvalidMovingException {
	if ((this.intersections.containsKey(origin))
		&& (this.intersections.containsKey(destination))) {
	    return verifyMoving((Intersection) this.intersections.get(origin),
		    (Intersection) this.intersections.get(destination));
	}
	return false;
    }

    public Color winnerIs() throws InvalidWinnerException {
	if (isFinished()) {
	    if (this.type == GameType.REGULAR) {
		if ((this.removedBlackRingNumber == 3)
			|| (this.removedBlackRingNumber > this.removedWhiteRingNumber))
		    return Color.BLACK;
		if ((this.removedWhiteRingNumber == 3)
			|| (this.removedBlackRingNumber < this.removedWhiteRingNumber)) {
		    return Color.WHITE;
		}
		return Color.NONE;
	    }

	    if (this.removedBlackRingNumber == 1)
		return Color.BLACK;
	    if (this.removedWhiteRingNumber == 1) {
		return Color.WHITE;
	    }
	    return Color.NONE;
	}

	throw new InvalidWinnerException("yinsh: game is not finished");
    }

    private void buildRow(char letter, int number, State state, Rows rows,
	    Row row) {
	Coordinates coordinates = new Coordinates(letter, number);
	Intersection intersection = (Intersection) this.intersections
		.get(coordinates);

	if ((!this.pStart) && (intersection.getState() == state)) {
	    this.pStart = true;
	    row.add(coordinates);
	} else if ((this.pStart) && (intersection.getState() == state)) {
	    row.add(coordinates);
	} else if ((this.pStart) && (intersection.getState() != state)) {
	    if (row.size() >= 5) {
		rows.add((Row) row.clone());
	    }
	    this.pStart = false;
	    row.clear();
	}
    }

    private void changeColor() {
	if (this.currentColor == Color.WHITE)
	    this.currentColor = Color.BLACK;
	else
	    this.currentColor = Color.WHITE;
    }

    private void flip(char letter, int number) {
	Coordinates coordinates = new Coordinates(letter, number);

	if (this.intersections.containsKey(coordinates))
	    ((Intersection) this.intersections.get(coordinates)).flip();
    }

    private void flip(Intersection origin, Intersection destination) {
	if (origin.getLetter() == destination.getLetter()) {
	    if (origin.getNumber() < destination.getNumber()) {
		int n = origin.getNumber() + 1;

		while (n < destination.getNumber()) {
		    flip(origin.getLetter(), n);
		    n++;
		}
	    } else {
		int n = origin.getNumber() - 1;

		while (n > destination.getNumber()) {
		    flip(origin.getLetter(), n);
		    n--;
		}
	    }
	} else if (origin.getNumber() == destination.getNumber()) {
	    if (origin.getLetter() < destination.getLetter()) {
		char l = (char) (origin.getLetter() + '\001');

		while (l < destination.getLetter()) {
		    flip(l, origin.getNumber());
		    l = (char) (l + '\001');
		}
	    } else {
		char l = (char) (origin.getLetter() - '\001');

		while (l > destination.getLetter()) {
		    flip(l, origin.getNumber());
		    l = (char) (l - '\001');
		}
	    }
	} else if (origin.getLetter() < destination.getLetter()) {
	    int n = origin.getNumber() + 1;
	    char l = (char) (origin.getLetter() + '\001');

	    while (l < destination.getLetter()) {
		flip(l, n);
		l = (char) (l + '\001');
		n++;
	    }
	} else {
	    int n = origin.getNumber() - 1;
	    char l = (char) (origin.getLetter() - '\001');

	    while (l > destination.getLetter()) {
		flip(l, n);
		l = (char) (l - '\001');
		n--;
	    }
	}
    }

    private Intersection getIntersection(char letter, int number) {
	Coordinates coordinates = new Coordinates(letter, number);

	if (this.intersections.containsKey(coordinates)) {
	    return (Intersection) this.intersections.get(coordinates);
	}
	return null;
    }

    private void removeBlackRing(Coordinates coordinates) {
	ListIterator<Coordinates> it = this.placedBlackRingCoordinates
		.listIterator();
	boolean found = false;

	while ((!found) && (it.hasNext())) {
	    Coordinates c = (Coordinates) it.next();

	    found = c.equals(coordinates);
	    if (found)
		this.placedBlackRingCoordinates.remove(c);
	}
    }

    private void removeWhiteRing(Coordinates coordinates) {
	ListIterator<Coordinates> it = this.placedWhiteRingCoordinates
		.listIterator();
	boolean found = false;

	while ((!found) && (it.hasNext())) {
	    Coordinates c = (Coordinates) it.next();

	    found = c.equals(coordinates);
	    if (found)
		this.placedWhiteRingCoordinates.remove(c);
	}
    }

    private void removeMarker(char letter, int number)
	    throws InvalidRemovingRowException {
	Coordinates coordinates = new Coordinates(letter, number);

	if (this.intersections.containsKey(coordinates)) {
	    ((Intersection) this.intersections.get(coordinates)).removeMarker();
	    this.markerNumber += 1;
	} else {
	    throw new InvalidRemovingRowException("yinsh: invalid coordinates");
	}
    }

    private void verifyIntersection(char letter, int number) {
	Coordinates coordinates = new Coordinates(letter, number);

	if (this.intersections.containsKey(coordinates)) {
	    State state = ((Intersection) this.intersections.get(coordinates))
		    .getState();

	    if ((state == State.BLACK_RING) || (state == State.WHITE_RING)) {
		this.pNoVacant = false;

		this.pOk = false;
	    } else if ((state == State.BLACK_MARKER)
		    || (state == State.WHITE_MARKER)) {
		this.pNoVacant = true;
	    } else if ((state == State.VACANT) && (this.pNoVacant)) {
		this.pOk = false;
	    }
	}
    }

    private void verifyIntersectionInRow(char letter, int number, Color color)
	    throws InvalidColorException {
	Coordinates coordinates = new Coordinates(letter, number);

	if ((this.intersections.containsKey(coordinates))
		&& (((Intersection) this.intersections.get(coordinates))
			.getColor() != color))
	    this.pOk = false;
    }

    private boolean verifyMoving(Intersection origin, Intersection destination)
	    throws InvalidMovingException {
	this.pOk = true;

	if ((origin.getState() != State.BLACK_MARKER_RING)
		&& (origin.getState() != State.WHITE_MARKER_RING)) {
	    throw new InvalidMovingException("yinsh: no ring at origin");
	}

	if ((origin == destination) || (destination.getState() != State.VACANT)) {
	    return false;
	}

	if (origin.getLetter() == destination.getLetter()) {
	    if (origin.getNumber() < destination.getNumber()) {
		int n = origin.getNumber() + 1;
		this.pNoVacant = false;
		do {
		    verifyIntersection(origin.getLetter(), n);
		    n++;

		    if (n >= destination.getNumber())
			break;
		} while (this.pOk);
	    } else {
		int n = origin.getNumber() - 1;
		this.pNoVacant = false;
		do {
		    verifyIntersection(origin.getLetter(), n);
		    n--;

		    if (n <= destination.getNumber())
			break;
		} while (this.pOk);
	    }

	} else if (origin.getNumber() == destination.getNumber()) {
	    if (origin.getLetter() < destination.getLetter()) {
		char l = (char) (origin.getLetter() + '\001');
		this.pNoVacant = false;
		do {
		    verifyIntersection(l, origin.getNumber());
		    l = (char) (l + '\001');

		    if (l >= destination.getLetter())
			break;
		} while (this.pOk);
	    } else {
		char l = (char) (origin.getLetter() - '\001');
		this.pNoVacant = false;
		do {
		    verifyIntersection(l, origin.getNumber());
		    l = (char) (l - '\001');

		    if (l <= destination.getLetter())
			break;
		} while (this.pOk);
	    }

	} else if (origin.getLetter() - destination.getLetter() == origin
		.getNumber() - destination.getNumber()) {
	    if (origin.getLetter() < destination.getLetter()) {
		int n = origin.getNumber() + 1;
		char l = (char) (origin.getLetter() + '\001');
		this.pNoVacant = false;
		do {
		    verifyIntersection(l, n);
		    l = (char) (l + '\001');
		    n++;

		    if (l >= destination.getLetter())
			break;
		} while (this.pOk);
	    } else {
		int n = origin.getNumber() - 1;
		char l = (char) (origin.getLetter() - '\001');
		this.pNoVacant = false;
		do {
		    verifyIntersection(l, n);
		    l = (char) (l - '\001');
		    n--;

		    if (l <= destination.getLetter())
			break;
		} while (this.pOk);
	    }

	} else {
	    this.pOk = false;
	}

	return this.pOk;
    }

    private boolean verifyRow(Intersection begin, Intersection end, Color color)
	    throws InvalidColorException {
	this.pOk = true;

	if (begin.getLetter() == end.getLetter()) {
	    if (begin.getNumber() < end.getNumber()) {
		int n = begin.getNumber() + 1;
		do {
		    verifyIntersectionInRow(begin.getLetter(), n, color);
		    n++;

		    if (n >= end.getNumber())
			break;
		} while (this.pOk);
	    } else {
		int n = begin.getNumber() - 1;
		do {
		    verifyIntersectionInRow(begin.getLetter(), n, color);
		    n--;

		    if (n <= end.getNumber())
			break;
		} while (this.pOk);
	    }

	} else if (begin.getNumber() == end.getNumber()) {
	    if (begin.getLetter() < end.getLetter()) {
		char l = (char) (begin.getLetter() + '\001');
		do {
		    verifyIntersectionInRow(l, begin.getNumber(), color);
		    l = (char) (l + '\001');

		    if (l >= end.getLetter())
			break;
		} while (this.pOk);
	    } else {
		char l = (char) (begin.getLetter() - '\001');
		do {
		    verifyIntersectionInRow(l, begin.getNumber(), color);
		    l = (char) (l - '\001');

		    if (l <= end.getLetter())
			break;
		} while (this.pOk);
	    }

	} else if (begin.getLetter() - end.getLetter() == begin.getNumber()
		- end.getNumber()) {
	    if (begin.getLetter() < end.getLetter()) {
		int n = begin.getNumber() + 1;
		char l = (char) (begin.getLetter() + '\001');
		do {
		    verifyIntersectionInRow(l, n, color);
		    l = (char) (l + '\001');
		    n++;

		    if (l >= end.getLetter())
			break;
		} while (this.pOk);
	    } else {
		int n = begin.getNumber() - 1;
		char l = (char) (begin.getLetter() - '\001');
		do {
		    verifyIntersectionInRow(l, n, color);
		    l = (char) (l - '\001');
		    n--;

		    if (l <= end.getLetter())
			break;
		} while (this.pOk);
	    }

	} else {
	    this.pOk = false;
	}

	return this.pOk;
    }

    private Intersections intersections;
    private GameType type;
    private Color currentColor;
    private CoordinatesList placedWhiteRingCoordinates;
    private CoordinatesList placedBlackRingCoordinates;
    private int removedBlackRingNumber;
    private int removedWhiteRingNumber;
    private int markerNumber;
    private boolean pOk;
    private boolean pNoVacant;
    private boolean pStart;
    private Phase phase;
}