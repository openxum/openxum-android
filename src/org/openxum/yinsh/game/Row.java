/**
 * @file org/openxum/yinsh/game/Row.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.yinsh.game;

import java.util.ListIterator;
import java.util.Vector;

public class Row extends Vector<Coordinates> {
    private static final long serialVersionUID = 3623458231807040022L;

    public Row() {
    }

    public Row(Coordinates begin, Coordinates end) {
	build(begin, end);
    }

    public boolean belong(Coordinates coordinates) {
	return contains(coordinates);
    }

    public void decode(String msg) {
	Coordinates begin = new Coordinates();
	Coordinates end = new Coordinates();
	String str[] = msg.split(";");

	begin.decode(str[0]);
	end.decode(str[1]);
	clear();
	build(begin, end);
    }

    public String encode() {
	String msg = new String();
	String begin = ((Coordinates) firstElement()).encode();
	String end = ((Coordinates) lastElement()).encode();

	msg = begin + ";";
	msg = msg + end;
	return msg;
    }

    public boolean isSeparated(Row row) {
	boolean found = false;
	ListIterator<Coordinates> it = row.listIterator();

	while (!found && it.hasNext()) {
	    found = contains(it.next());
	}
	return !found;
    }

    @Override
    public String toString() {
	String str = new String();
	ListIterator<Coordinates> it = listIterator();

	str = "{ ";
	while (it.hasNext()) {
	    Coordinates coordinates = (Coordinates) it.next();
	    str = str + coordinates.getLetter() + coordinates.getNumber() + " ";
	}
	str = str + "}";
	return str;
    }

    private void build(Coordinates begin, Coordinates end) {
	if (begin.getLetter() == end.getLetter()) {
	    if (begin.getNumber() < end.getNumber()) {
		int n = begin.getNumber();

		while (n <= end.getNumber()) {
		    add(new Coordinates(begin.getLetter(), n));
		    n++;
		}
	    } else {
		int n = begin.getNumber();

		while (n >= end.getNumber()) {
		    add(new Coordinates(begin.getLetter(), n));
		    n--;
		}
	    }
	} else if (begin.getNumber() == end.getNumber()) {
	    if (begin.getLetter() < end.getLetter()) {
		char l = begin.getLetter();

		while (l <= end.getLetter()) {
		    add(new Coordinates(l, begin.getNumber()));
		    l = (char) (l + '\001');
		}
	    } else {
		char l = begin.getLetter();

		while (l >= end.getLetter()) {
		    add(new Coordinates(l, begin.getNumber()));
		    l = (char) (l - '\001');
		}
	    }
	} else if (begin.getLetter() < end.getLetter()) {
	    int n = begin.getNumber();
	    char l = begin.getLetter();

	    while (l <= end.getLetter()) {
		add(new Coordinates(l, n));
		l = (char) (l + '\001');
		n++;
	    }
	} else {
	    int n = begin.getNumber();
	    char l = begin.getLetter();

	    while (l >= end.getLetter()) {
		add(new Coordinates(l, n));
		l = (char) (l - '\001');
		n--;
	    }
	}
    }
}