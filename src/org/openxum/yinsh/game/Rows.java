/**
 * @file org/openxum/yinsh/game/Rows.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.yinsh.game;

import java.util.ListIterator;
import java.util.Vector;

public class Rows extends Vector<Row> {
    private static final long serialVersionUID = 2525717715369124785L;
    
    public void decode(String msg) {
	String str[] = msg.split(",");
	
	for (int i = 0; i < str.length; ++i) {
	    Row row = new Row();
	    
	    row.decode(str[i]);
	    this.add(row);
	}
    }
    
    public String encode() {
	String str = new String();
	ListIterator<Row> it = listIterator();

	while (it.hasNext()) {
	    Row row = (Row) it.next();
	    str = str + row.encode() + ",";
	}
	return str;
    }
}