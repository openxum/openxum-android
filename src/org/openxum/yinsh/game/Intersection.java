/**
 * @file org/openxum/yinsh/game/Intersection.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.yinsh.game;

import org.openxum.common.Color;
import org.openxum.common.InvalidColorException;
import org.openxum.common.InvalidMovingException;

public class Intersection {

    public Intersection(Coordinates coordinates) {
	this.coordinates = coordinates;
	this.state = State.VACANT;
    }
    
    public Intersection(Intersection intersection) {
	this.coordinates = intersection.coordinates;
	this.state = intersection.state;	
    }

    public void flip() {
	if (this.state == State.BLACK_MARKER)
	    this.state = State.WHITE_MARKER;
	else if (this.state == State.WHITE_MARKER)
	    this.state = State.BLACK_MARKER;
    }

    public Color getColor() throws InvalidColorException {
	if (this.state == State.VACANT) {
	    throw new InvalidColorException("yinsh: no color");
	}

	if ((this.state == State.BLACK_RING)
		|| (this.state == State.BLACK_MARKER)
		|| (this.state == State.BLACK_MARKER_RING)) {
	    return Color.BLACK;
	}
	return Color.WHITE;
    }

    public Coordinates getCoordinates() {
	return this.coordinates;
    }

    public char getLetter() {
	return this.coordinates.getLetter();
    }

    public int getNumber() {
	return this.coordinates.getNumber();
    }

    public State getState() {
	return this.state;
    }

    public void putMarker(Color color) throws InvalidMovingException {
	if (color == Color.BLACK) {
	    if (this.state == State.BLACK_RING)
		this.state = State.BLACK_MARKER_RING;
	    else {
		throw new InvalidMovingException(
			"yinsh: invalid color marker on "
				+ this.coordinates.toString());
	    }
	} else if (this.state == State.WHITE_RING)
	    this.state = State.WHITE_MARKER_RING;
	else
	    throw new InvalidMovingException("yinsh: invalid color marker on "
		    + this.coordinates.toString());
    }

    public void putRing(Color color) {
	if (color == Color.BLACK)
	    this.state = State.BLACK_RING;
	else
	    this.state = State.WHITE_RING;
    }

    public boolean equals(Object obj) {
	return this.coordinates.equals(obj);
    }

    public void removeMarker() {
	this.state = State.VACANT;
    }

    public void removeRing() throws InvalidMovingException {
	if ((this.state == State.BLACK_MARKER_RING)
		|| (this.state == State.WHITE_MARKER_RING)) {
	    if (this.state == State.BLACK_MARKER_RING)
		this.state = State.BLACK_MARKER;
	    else
		this.state = State.WHITE_MARKER;
	} else
	    throw new InvalidMovingException("yinsh: invalid ring");
    }

    public void removeRingBoard() {
	this.state = State.VACANT;
    }

    public String toString() {
	switch (state) {
	case BLACK_MARKER:
	    return "[BM ]";
	case WHITE_MARKER:
	    return "[WM ]";
	case BLACK_RING:
	    return "[BR ]";
	case WHITE_RING:
	    return "[WR ]";
	case BLACK_MARKER_RING:
	    return "[BMR]";
	case WHITE_MARKER_RING:
	    return "[WMR]";
	case VACANT: 
	    return "[V  ]";
	}
	return "";
    }

    private Coordinates coordinates;
    private State state;
}
