/**
 * @file org/openxum/yinsh/game/Coordinates.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.yinsh.game;

public class Coordinates extends org.openxum.common.Coordinates {
    public Coordinates() {
    }

    public Coordinates(char letter, int number) {
	super(letter, number);
    }

    public Coordinates(Coordinates coordinates) {
	super(coordinates.getLetter(), coordinates.getNumber());
    }
    
    public void decode(String msg) {
	this.letter = msg.charAt(0);
	if (msg.length() == 2)
	    this.number = (msg.charAt(1) - '0');
	else
	    this.number = (msg.charAt(2) - '0' + 10);
    }

    public String encode() {
	String msg = new String();
	
	msg = msg + this.letter;
	if (this.number < 10) {
	    msg = msg + (char) (48 + this.number);
	} else {
	    msg = msg + '1';
	    msg = msg + (char) (48 + (this.number - 10));
	}
	return msg;
    }

    public int hashCode() {
	return this.letter - 'A' + (this.number - 1) * 11;
    }

    public boolean isValid() {
	return (this.letter >= 'A') && (this.letter <= 'K')
		&& (this.number >= 1) && (this.number <= 11);
    }

    public String toString() {
	return "Coordinates: " + this.letter + "," + this.number;
    }
}