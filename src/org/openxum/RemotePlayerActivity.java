package org.openxum;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.openxum.common.Color;
import org.openxum.yinsh.gui.YinshPlayerAdapter;
import org.openxum.yinsh.gui.YinshRemotePlayer;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class RemotePlayerActivity extends Activity {
    public static final int RETURN_CODE = 0;

    private List<YinshRemotePlayer> buildRemotePlayerList(HttpResponse response) {
	List<YinshRemotePlayer> players = null;
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

	try {
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    Document doc = builder.parse(response.getEntity().getContent());
	    Element root = doc.getDocumentElement();

	    if (root.getNodeName().compareTo("list") == 0) {
		NodeList gameNodeList = root.getChildNodes();

		players = new ArrayList<YinshRemotePlayer>();
		for (int i = 0; i < gameNodeList.getLength(); i++) {
		    Node player = gameNodeList.item(i);
		    String remoteLogin = player.getAttributes()
			    .getNamedItem("login").getNodeValue();

		    if (remoteLogin.compareTo(login) != 0) {
			YinshRemotePlayer remotePlayer = new YinshRemotePlayer(
				remoteLogin, player.getAttributes()
					.getNamedItem("color").getNodeValue()
					.compareTo("0") == 0 ? Color.BLACK
					: Color.WHITE, Integer.parseInt(player
					.getAttributes().getNamedItem("id")
					.getNodeValue()));

			players.add(remotePlayer);
		    }
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return players;
    }

    private void choiceColor() {
	AlertDialog.Builder alert = new AlertDialog.Builder(this);

	alert.setTitle("Choose:");
	CharSequence[] items = { "black", "white" };
	alert.setSingleChoiceItems(items, 0,
		new DialogInterface.OnClickListener() {

		    public void onClick(DialogInterface dialog, int item) {
		    }
		});

	alert.setPositiveButton("Valid", new DialogInterface.OnClickListener() {

	    public void onClick(DialogInterface dialog, int id) {
		String color = (id == 0 ? "black" : "white");
		HttpGet request = new HttpGet(OPENXUM_URI
			+ "?game=yinsh&action=new&login=" + login + "&color="
			+ color);
		
		try {
		    HttpResponse response = client.execute(request);

		    if (parseResponse(response, "id_waiting") != -1) {
			Toast.makeText(getApplicationContext(), "New game",
				Toast.LENGTH_LONG).show();
		    } else {
			Toast.makeText(getApplicationContext(),
				"Error: opened game exists already!",
				Toast.LENGTH_LONG).show();
		    }
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});

	alert.setNegativeButton("Cancel",
		new DialogInterface.OnClickListener() {

		    public void onClick(DialogInterface dialog, int id) {
			dialog.cancel();
		    }
		});

	AlertDialog ad = alert.create();
	ad.show();
    }

    private boolean connect() {
	HttpPost httppost = new HttpPost(OPENXUM_URI);
	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

	nameValuePairs.add(new BasicNameValuePair("action", "connect"));
	nameValuePairs.add(new BasicNameValuePair("login", login));
	nameValuePairs.add(new BasicNameValuePair("password", password));
	try {
	    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	    return parseStatus(client.execute(httppost));
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return false;
    }

    private int joinGame(int id_waiting, Color oponentColor) {
	HttpGet request = new HttpGet(OPENXUM_URI
		+ "?game=yinsh&action=join&id_waiting=" + id_waiting
		+ "&login=" + login);

	try {
	    HttpResponse response = client.execute(request);
	    int id_game = parseResponse(response, "id_game");

	    if (id_game != -1) {
		return id_game;
	    } else {
		Toast.makeText(getApplicationContext(),
			"Error: this game isn't available!", Toast.LENGTH_LONG)
			.show();
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return -1;
    }

    private int parseResponse(HttpResponse response, String name) {
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

	try {
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    Document doc = builder.parse(response.getEntity().getContent());
	    Element root = doc.getDocumentElement();

	    if (root.getNodeName().compareTo("response") == 0) {
		return Integer.parseInt(root.getAttributes().getNamedItem(name)
			.getNodeValue());
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return -1;
    }

    private boolean parseStatus(HttpResponse response) {
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

	try {
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    Document doc = builder.parse(response.getEntity().getContent());
	    Element root = doc.getDocumentElement();

	    if (root.getNodeName().compareTo("status") == 0) {
		return (root.getAttributes().getNamedItem("value")
			.getNodeValue().compareTo("OK") == 0);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.remote_player);

	client = new DefaultHttpClient();

	SharedPreferences prefs = PreferenceManager
		.getDefaultSharedPreferences(this);

	login = prefs.getString("login", "");
	password = prefs.getString("password", "");

	if (connect()) {
	    HttpGet request = new HttpGet(OPENXUM_URI
		    + "?game=yinsh&action=list_waiting");

	    try {
		HttpResponse response = client.execute(request);

		players = buildRemotePlayerList(response);
		remotePlayerList = (ListView) findViewById(R.id.remotePlayerListView);
		remotePlayerList.setAdapter(new YinshPlayerAdapter(this,
			players));
	    } catch (Exception e) {
		e.printStackTrace();
	    }

	    remotePlayerList.setOnItemClickListener(new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view,
			int index, long id) {
		    String color = (players.get(index).getColor() == Color.BLACK ? "white"
			    : "back");

		    GameActivity.currentGameID = joinGame(players.get(index)
			    .getID(), players.get(index).getColor());
		    if (GameActivity.currentGameID != -1) {
			Toast.makeText(
				getApplicationContext(),
				"Join game against "
					+ players.get(index).getName()
					+ " with " + color, Toast.LENGTH_LONG)
				.show();
			Intent intentPreferences = new Intent(
				RemotePlayerActivity.this, GameActivity.class);
			startActivityForResult(intentPreferences, RETURN_CODE);

		    } else {
			Toast.makeText(
				getApplicationContext(),
				"Error to join game against "
					+ players.get(index).getName(),
				Toast.LENGTH_LONG).show();
		    }
		}
	    });

	    newGameButton = (Button) findViewById(R.id.newGameButton);
	    newGameButton.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View view) {
		    choiceColor();
		}
	    });
	} else {
	    Toast.makeText(getApplicationContext(),
		    "Error to connect to game server", Toast.LENGTH_LONG)
		    .show();
	}
    }

    private DefaultHttpClient client;
    private ListView remotePlayerList;
    private Button newGameButton;
    private String login;
    private String password;
    List<YinshRemotePlayer> players;
    private final static String OPENXUM_URI = "http://www.openxum.org/remote/index.php";
}
