/**
 * @file org/openxum/GameActivity.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.openxum.common.Color;
import org.openxum.common.InvalidColorException;
import org.openxum.common.InvalidMovingException;
import org.openxum.common.InvalidWinnerException;
import org.openxum.yinsh.gui.Phase;
import org.openxum.yinsh.game.*;
import org.openxum.yinsh.player.*;
import org.openxum.yinsh.*;

public class GameActivity extends Activity {

    public void initGame() {
	this.boardGui = (org.openxum.yinsh.gui.BoardView) findViewById(R.id.board_yinsh);
	this.boardGui.setActivity(this);

	SharedPreferences prefs = PreferenceManager
		.getDefaultSharedPreferences(this);

	GameType gameType = (prefs.getString("game_type", "Regular").compareTo(
		"Regular") == 0) ? GameType.REGULAR : GameType.BLITZ;
	Color playerColor = (prefs.getString("player_color", "Black")
		.compareTo("Black") == 0) ? Color.BLACK : Color.WHITE;
	Color otherColor = (playerColor == Color.BLACK) ? Color.WHITE
		: Color.BLACK;

	judge = new Judge(gameType, Color.WHITE);
	player = new GuiPlayer(boardGui, gameType, Color.WHITE, playerColor);

	if (prefs.getString("oponent_type", "Computer").compareTo("Computer") == 0) {
	    otherPlayer = new MCTSPlayer(gameType, Color.WHITE, otherColor);
	} else if (prefs.getString("oponent_type", "Computer").compareTo("GUI") == 0) {
	    otherPlayer = new GuiPlayer(boardGui, gameType, Color.WHITE,
		    otherColor);
	} else if (prefs.getString("oponent_type", "Computer").compareTo(
		"Remote") == 0) {
	    otherPlayer = new RemotePlayer(this, gameType, Color.WHITE,
		    otherColor);
	}
	if (player.getColor() != judge.getCurrentColor()) {
	    phase = Phase.PUT_RING_OTHER;
	    if (!otherPlayer.isGUI()) {
		try {
		    putRingOther();
		} catch (InvalidPuttingException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
	    }
	} else {
	    phase = Phase.PUT_RING;
	}
	selectedRows = new Rows();

	this.boardGui.invalidate();
    }

    @Override
    public void finish() {
	player.finish();
	otherPlayer.finish();

	String msg = "no winner";

	try {
	    msg = this.judge.getBoard().winnerIs().toString() + " wins !";
	} catch (InvalidWinnerException e) {
	}

	Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	super.finish();
    }

    public Color getCurrentColor() {
	return this.judge.getBoard().getCurrentColor();
    }

    public boolean existIntersection(char letter, int number) {
	return judge.existIntersection(letter, number);
    }

    public Intersections getIntersections() {
	return judge.getIntersections();
    }

    public State getIntersectionState(char letter, int number) {
	return judge.getIntersectionState(letter, number);
    }

    public org.openxum.yinsh.gui.Phase getPhase() {
	return phase;
    }

    public Color getPlayerColor() {
	return this.player.getColor();
    }

    public PossibleMovingList getPossibleMovingList(Coordinates origin,
	    Color color, boolean control) throws InvalidMovingException {
	return judge.getBoard().getPossibleMovingList(origin, color, control);
    }

    public int getRemovedRingNumber(Color color) {
	return judge.getRemovedRingNumber(color);
    }

    public SeparatedRows getRows(Color color) {
	return judge.getBoard().getRows(color);
    }

    public void moveRing() throws InvalidMovingException,
	    InvalidPuttingException, InvalidRemovingRowException,
	    InvalidRemovingRingException, InvalidColorException,
	    InvalidWinnerException {
	Color color = judge.getCurrentColor();
	org.openxum.yinsh.game.Coordinates destination = player
		.moveRing(origin);

	otherPlayer.moveRing(origin, destination, color);
	judge.getBoard().moveRing(origin, destination);
	if (judge.getBoard().getRows(color).size() == 0) {
	    otherPlayer.removeNoRow(color);
	    judge.getBoard().removeNoRow();
	    player.removeNoRow(color);
	    if (judge.getBoard().getRows(otherPlayer.getColor()).size() > 0) {
		before = true;
		phase = Phase.REMOVE_ROW_OTHER;
		if (!otherPlayer.isGUI()) {
		    if (otherPlayer.isRemote()) {
			((RemotePlayer) otherPlayer).waitRemoveRow();
		    } else {
			removeRowOther();
		    }
		}
	    } else {
		phase = Phase.PUT_MARKER_OTHER;
		if (!otherPlayer.isGUI()) {
		    if (otherPlayer.isRemote()) {
			((RemotePlayer) otherPlayer).waitPutMarker();
		    } else {
			putMarkerOther();
		    }
		}
	    }
	} else {
	    phase = Phase.REMOVE_ROW;
	}
    }

    public void moveRingOther() throws InvalidMovingException,
	    InvalidPuttingException, InvalidRemovingRowException,
	    InvalidRemovingRingException, InvalidColorException,
	    InvalidWinnerException {
	Color color = judge.getCurrentColor();
	org.openxum.yinsh.game.Coordinates destination = otherPlayer
		.moveRing(origin);

	player.moveRing(origin, destination, color);
	judge.getBoard().moveRing(origin, destination);
	if (judge.getBoard().getRows(color).size() == 0) {
	    if (judge.getBoard().getRows(player.getColor()).size() > 0) {
		phase = Phase.REMOVE_ROW;
	    } else {
		otherPlayer.removeNoRow(color);
		judge.getBoard().removeNoRow();
		player.removeNoRow(color);
		phase = Phase.PUT_MARKER;
	    }
	} else {
	    before = false;
	    phase = Phase.REMOVE_ROW_OTHER;
	    if (!otherPlayer.isGUI()) {
		if (otherPlayer.isRemote()) {
		    ((RemotePlayer) otherPlayer).waitRemoveRow();
		} else {
		    removeRowOther();
		}
	    }
	}
    }

    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.main_yinsh);

	initGame();
    }

    public void putMarker() throws InvalidMovingException,
	    InvalidPuttingException, InvalidRemovingRowException,
	    InvalidRemovingRingException, InvalidColorException,
	    InvalidWinnerException {
	Color color = judge.getCurrentColor();

	origin = player.putMarker();
	if (judge.getBoard().getPossibleMovingList(origin, color, false).size() > 0) {
	    otherPlayer.putMarker(origin, color);
	    judge.getBoard().putMarker(origin, color);
	    phase = Phase.MOVE_RING;
	} else {
	}
    }

    public void putMarkerOther() throws InvalidMovingException,
	    InvalidPuttingException, InvalidRemovingRowException,
	    InvalidRemovingRingException, InvalidColorException,
	    InvalidWinnerException {
	Color color = judge.getCurrentColor();

	origin = otherPlayer.putMarker();
	if (judge.getBoard().getPossibleMovingList(origin, color, false).size() > 0) {
	    player.putMarker(origin, color);
	    judge.getBoard().putMarker(origin, color);
	    phase = Phase.MOVE_RING_OTHER;
	    if (!otherPlayer.isGUI()) {
		if (otherPlayer.isRemote()) {
		    ((RemotePlayer) otherPlayer).waitMoveRing();
		} else {
		    moveRingOther();
		}
	    }
	} else {
	}
    }

    public void putRing() throws InvalidPuttingException,
	    InvalidMovingException, InvalidRemovingRowException,
	    InvalidRemovingRingException, InvalidColorException,
	    InvalidWinnerException {
	Color color = player.getColor();
	org.openxum.yinsh.game.Coordinates coordinates = player.putRing();

	otherPlayer.putRing(coordinates, color);
	judge.getBoard().putRing(coordinates, color);

	if (judge.getBoard().isInitialized()) {
	    phase = Phase.PUT_MARKER_OTHER;
	    if (!otherPlayer.isGUI()) {
		if (otherPlayer.isRemote()) {
		    ((RemotePlayer) otherPlayer).waitPutMarker();
		} else {
		    putMarkerOther();
		}
	    }
	} else {
	    phase = Phase.PUT_RING_OTHER;
	    if (!otherPlayer.isGUI()) {
		if (otherPlayer.isRemote()) {
		    ((RemotePlayer) otherPlayer).waitPutRing();
		} else {
		    putRingOther();
		}
	    }
	}
    }

    public void putRingOther() throws InvalidPuttingException {
	org.openxum.yinsh.game.Coordinates otherCoordinates = otherPlayer
		.putRing();

	player.putRing(otherCoordinates, judge.getCurrentColor());
	judge.getBoard().putRing(otherCoordinates, judge.getCurrentColor());
	if (judge.getBoard().isInitialized()) {
	    phase = Phase.PUT_MARKER;
	} else {
	    phase = Phase.PUT_RING;
	}
	this.boardGui.invalidate();
    }

    public void removeRing() throws InvalidRemovingRingException,
	    InvalidColorException, InvalidMovingException,
	    InvalidPuttingException, InvalidRemovingRowException,
	    InvalidWinnerException {
	Color color = player.getColor();
	Coordinates coordinates = player.removeRing();

	otherPlayer.removeRing(coordinates, color);
	judge.getBoard().removeRing(coordinates, color);
	if (judge.isFinished()) {
	    finish();
	} else {
	    if (judge.getBoard().getRows(otherPlayer.getColor()).size() > 0) {
		before = true;
		phase = Phase.REMOVE_ROW_OTHER;
		if (!otherPlayer.isGUI()) {
		    if (otherPlayer.isRemote()) {
			((RemotePlayer) otherPlayer).waitRemoveRow();
		    } else {
			removeRowOther();
		    }
		}
	    } else {
		phase = Phase.PUT_MARKER_OTHER;
		if (!otherPlayer.isGUI()) {
		    if (otherPlayer.isRemote()) {
			((RemotePlayer) otherPlayer).waitPutMarker();
		    } else {
			putMarkerOther();
		    }
		}
	    }
	}
    }

    public void removeRingOther() throws InvalidRemovingRingException,
	    InvalidColorException, InvalidMovingException,
	    InvalidPuttingException, InvalidRemovingRowException,
	    InvalidWinnerException {
	Color color = otherPlayer.getColor();
	Coordinates coordinates = otherPlayer.removeRing();

	player.removeRing(coordinates, color);
	judge.getBoard().removeRing(coordinates, color);
	if (judge.isFinished()) {
	    finish();
	} else {
	    if (judge.getBoard().getRows(player.getColor()).size() > 0) {
		phase = Phase.REMOVE_ROW;
	    } else {
		if (before) {
		    phase = Phase.PUT_MARKER_OTHER;
		    if (!otherPlayer.isGUI()) {
			if (otherPlayer.isRemote()) {
			    ((RemotePlayer) otherPlayer).waitPutMarker();
			} else {
			    putMarkerOther();
			}
		    }
		} else {
		    phase = Phase.PUT_MARKER;
		}
	    }
	}
    }

    public void removeRow() throws InvalidRemovingRowException {
	Color color = player.getColor();
	SeparatedRows srows = judge.getBoard().getRows(color);

	player.removeRows(selectedRows);
	if (selectedRows.size() == srows.size()) {
	    otherPlayer.removeRows(selectedRows, color);
	    judge.getBoard().removeRows(selectedRows, color);
	    selectedRows.clear();
	    phase = Phase.REMOVE_RING;
	}
    }

    public void removeRowOther() throws InvalidRemovingRowException,
	    InvalidRemovingRingException, InvalidColorException,
	    InvalidMovingException, InvalidPuttingException,
	    InvalidWinnerException {
	Color player_color = judge.getCurrentColor();
	Rows rows = new Rows();

	otherPlayer.removeRows(rows);
	judge.getBoard().removeRows(rows, player_color);
	player.removeRows(rows, player_color);
	phase = Phase.REMOVE_RING_OTHER;
	if (!otherPlayer.isGUI()) {
	    if (otherPlayer.isRemote()) {
		((RemotePlayer) otherPlayer).waitRemoveRing();
	    } else {
		removeRingOther();
	    }
	}
    }

    public void setPhase(org.openxum.yinsh.gui.Phase phase) {
	this.phase = phase;
    }

    public boolean verifyMoving(org.openxum.yinsh.game.Coordinates origin,
	    org.openxum.yinsh.game.Coordinates destination)
	    throws InvalidMovingException {
	return judge.getBoard().verifyMoving(origin, destination);
    }

    private org.openxum.yinsh.gui.BoardView boardGui;
    private org.openxum.yinsh.gui.Phase phase;
    private org.openxum.yinsh.player.Player player;
    private org.openxum.yinsh.player.Player otherPlayer;
    private org.openxum.yinsh.Judge judge;
    private org.openxum.yinsh.game.Coordinates origin;
    private org.openxum.yinsh.game.Rows selectedRows;
    private boolean before;

    public static int currentGameID = -1;
}