/**
 * @file org/openxum/OpenxumActivity.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum;

import org.openxum.gui.MainView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class OpenxumActivity extends Activity {
    public static final int RETURN_CODE = 0;

    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.main);
	
	MainView view = (MainView) findViewById(R.id.main_page);
	
	view.setActivity(this);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
	new MenuInflater(this).inflate(R.menu.main_menu, menu);
	return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
	switch (item.getItemId()) {
	case R.id.game:
	    Intent intentGame = new Intent(this, GameActivity.class);
	    startActivityForResult(intentGame, 0);
	    return true;
	case R.id.preferences:
	    Intent intentPreferences = new Intent(this, Preferences.class);
	    startActivityForResult(intentPreferences, 0);
	    return true;
	case R.id.about:
	    Intent intentAbout = new Intent(this, About.class);
	    startActivityForResult(intentAbout, 0);
	    return true;
	case R.id.quit:
	    finish();
	    return true;
	}
	return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
	super.onSaveInstanceState(outState);

	System.out.println("onSaveInstanceState");

    }

    private SharedPreferences prefs;
}