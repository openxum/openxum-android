package org.openxum.gui;

import org.openxum.GameActivity;
import org.openxum.OpenxumActivity;
import org.openxum.R;
import org.openxum.RemotePlayerActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Toast;

public class MainView extends View implements OnTouchListener {

    public MainView(Context context) {
	super(context);
	this.setOnTouchListener(this);
    }

    public MainView(Context context, AttributeSet attrs) {
	super(context, attrs);
	this.setOnTouchListener(this);
    }

    public MainView(Context context, AttributeSet attrs, int defStyle) {
	super(context, attrs, defStyle);
	this.setOnTouchListener(this);
    }

    @SuppressLint("DrawAllocation")
    public void onDraw(Canvas canvas) {
	super.onDraw(canvas);

	// background
	float y_start = getHeight() * 0.15f;
	float y_end = getHeight() * 0.9f;
	float x_start = getWidth() * 0.02f;
	float x_end = getWidth() * 0.98f;

	if (getHeight() < getWidth()) { // landscape
	    Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
		    R.drawable.main_landscape);

	    canvas.drawBitmap(bitmap, null, new Rect(0, 0, getWidth(),
		    getHeight()), paint);

	    float delta_x = getWidth() * 0.96f / 3.f;
	    float y = getHeight() * 0.75f / 2.f + y_start;

	    for (int i = 0; i <= 3; ++i) {
		canvas.drawLine(x_start + i * delta_x, y_start, x_start + i
			* delta_x, y_end, paint);
	    }
	    canvas.drawLine(x_start, y_start, x_end, y_start, paint);
	    canvas.drawLine(x_start, y, x_end, y, paint);
	    canvas.drawLine(x_start, y_end, x_end, y_end, paint);

	} else {
	    Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
		    R.drawable.main);

	    canvas.drawBitmap(bitmap, null, new Rect(0, 0, getWidth(),
		    getHeight()), paint);

	    float x = getWidth() / 2.f;
	    float delta_y = getHeight() * 0.25f;

	    for (int i = 0; i <= 3; ++i) {
		canvas.drawLine(x_start, y_start + i * delta_y, x_end, y_start
			+ i * delta_y, paint);
	    }
	    canvas.drawLine(x_start, y_start, x_start, y_end, paint);
	    canvas.drawLine(x, y_start, x, y_end, paint);
	    canvas.drawLine(x_end, y_start, x_end, y_end, paint);
	}
    }

    public boolean onTouch(View v, MotionEvent event) {
	int action = event.getAction();

	if (action == MotionEvent.ACTION_DOWN) {
	} else if (action == MotionEvent.ACTION_UP) {
	    float xTouch = event.getX();
	    float yTouch = event.getY();
	    float y_start = getHeight() * 0.15f;
	    float y_end = getHeight() * 0.9f;
	    float x_start = getWidth() * 0.02f;
	    float x_end = getWidth() * 0.98f;

	    if (yTouch > y_start && yTouch < y_end) {
		GameName game = GameName.NONE;

		if (getHeight() < getWidth()) { // landscape
		    float delta_x = getWidth() * 0.96f / 3.f;
		    float y = getHeight() * 0.75f / 2.f + y_start;

		    if (xTouch > x_start + delta_x
			    && xTouch < x_start + 2 * delta_x && yTouch > y
			    && yTouch < y_end) {
			game = GameName.YINSH;
		    }
		} else {
		    float x = getWidth() / 2.f;
		    float delta_y = getHeight() * 0.25f;

		    if (xTouch > x_start && xTouch < x
			    && yTouch > y_start + 2 * delta_y && yTouch < y_end) {
			game = GameName.YINSH;
		    }
		}

		if (game == GameName.YINSH) {
		    prefs = PreferenceManager
			    .getDefaultSharedPreferences(getContext());

		    if (prefs.getString("oponent_type", "Computer").compareTo(
			    "Remote") == 0) {
			if (isNetworkAvailable()) {
			    Intent intentPreferences = new Intent(activity,
				    RemotePlayerActivity.class);
			    activity.startActivityForResult(intentPreferences,
				    OpenxumActivity.RETURN_CODE);
			} else {
			    Toast.makeText(
				    getContext(),
				    "No available network: change oponent type",
				    Toast.LENGTH_SHORT).show();
			}
		    } else {
			Intent intentPreferences = new Intent(activity,
				GameActivity.class);
			activity.startActivityForResult(intentPreferences,
				OpenxumActivity.RETURN_CODE);
		    }
		} else {
		    Toast.makeText(getContext(),
			    "This game is not available on Android version!",
			    Toast.LENGTH_SHORT).show();
		}
	    }
	} else if (action == MotionEvent.ACTION_MOVE) {
	}
	return true;
    }

    public void setActivity(OpenxumActivity activity) {
	this.activity = activity;
    }

    private boolean isNetworkAvailable() {
	ConnectivityManager connectivityManager = (ConnectivityManager) activity
		.getSystemService(Context.CONNECTIVITY_SERVICE);
	NetworkInfo activeNetworkInfo = connectivityManager
		.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    private enum GameName {
	NONE, GIPF, INVERS, DVONN, ZERTZ, TZAAR, YINSH
    }

    private Paint paint = new Paint();
    private OpenxumActivity activity;
    private SharedPreferences prefs;
}
