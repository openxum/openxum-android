/**
 * @file org/openxum/gui/BoardView.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.gui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public abstract class BoardView extends View {

    public BoardView(Context context) {
	super(context);
    }

    public BoardView(Context context, AttributeSet attrs) {
	super(context, attrs);
    }

    public BoardView(Context context, AttributeSet attrs, int defStyle) {
	super(context, attrs, defStyle);
    }

    protected abstract void computeCoordinates(char letter, int number, Point pt);

    protected char computeLetter(int x, int y) {
	char letter = 'X';

	int index = (x - offset_x) / delta_x;
	int x_ref = offset_x + delta_x * index;
	int x_ref_2 = offset_x + delta_x * (index + 1);

	if (x < offset_x) {
	    letter = 'A';
	} else if (x <= x_ref + delta_x / 2 && x >= x_ref
		&& x <= x_ref + TOLERANCE) {
	    letter = (char) ('A' + index);
	} else if (x > x_ref + delta_x / 2 && x >= x_ref_2 - TOLERANCE) {
	    letter = (char) ('A' + (index + 1));
	}
	return letter;
    }

    protected int computeNumber(int x, int y) {
	int number = -1;
	Point pt = new Point();

	computeCoordinates('A', 1, pt);

	// translation to A1 and rotation
	int X = x - pt.x;
	int Y = y - pt.y;
	double sin_alpha = 1. / Math.sqrt(5.);
	double cos_alpha = 2. * sin_alpha;

	int x2 = (int) (X * sin_alpha - Y * cos_alpha) + pt.x;
	int delta_x2 = (int) (delta_x * cos_alpha);

	int index = (x2 - offset_x) / delta_x2;
	int x_ref = offset_x + delta_x2 * index;
	int x_ref_2 = offset_x + delta_x2 * (index + 1);

	if (x2 > 0 && x2 < offset_x) {
	    number = 1;
	} else if (x2 <= x_ref + delta_x2 / 2 && x2 >= x_ref
		&& x2 <= x_ref + TOLERANCE) {
	    number = index + 1;
	} else if (x2 > x_ref + delta_x2 / 2 && x2 >= x_ref_2 - TOLERANCE) {
	    number = index + 2;
	}
	return number;
    }

    protected void drawRing(Canvas canvas, Point pt, float radius,
	    org.openxum.common.Color color, boolean selected) {
	int width = (int) (radius / 5.);
	int pos = (int) (radius / 3.);
	Rect r = new Rect(pt.x - pos, pt.y - pos, pt.x + pos, pt.y + pos);
	Paint paint = new Paint();

	if (color == org.openxum.common.Color.BLACK) {
	    paint.setColor(Color.DKGRAY);
	} else if (color == org.openxum.common.Color.WHITE) {
	    paint.setColor(Color.LTGRAY);
	}
	paint.setStyle(Paint.Style.STROKE);
	paint.setStrokeWidth(width);
	canvas.drawArc(new RectF(r), 0, 360, false, paint);
	paint.setColor(Color.BLACK);
	paint.setStyle(Paint.Style.STROKE);
	paint.setStrokeWidth(1);
	canvas.drawCircle(pt.x, pt.y, pos - width / 2, paint);

	if (selected) {
	    paint.setColor(Color.RED);
	    paint.setStrokeWidth(3);
	    canvas.drawCircle(pt.x, pt.y, pos + width / 2, paint);
	} else {
	    canvas.drawCircle(pt.x, pt.y, pos + width / 2, paint);
	}
    }

    public void setActivity(org.openxum.GameActivity activity) {
	this.activity = activity;
    }

    private static final int TOLERANCE = 30;

    protected org.openxum.GameActivity activity;
    protected int delta_x;
    protected int delta_y;
    protected int delta_xy;
    protected int offset_x;
    protected int offset_y;
}
