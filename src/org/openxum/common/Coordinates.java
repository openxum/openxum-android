/**
 * @file org/openxum/yinsh/common/Coordinates.java
 * 
 * OpenXum - An open multi-platform turn-based strategy game framework
 * This file is a part of the OpenXum environment
 * http://openxum.sourceforge.net
 *
 * Copyright (C) 2011-2012 Eric Ramat eramat@users.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openxum.common;

public abstract class Coordinates {
    protected char letter;
    protected int number;

    public Coordinates() {
	this.letter = 'A';
	this.number = 1;
    }

    public Coordinates(char letter, int number) {
	this.letter = letter;
	this.number = number;
    }

    public abstract void decode(String paramString);

    int distance(Coordinates coordinates) {
	if (coordinates.getLetter() == this.letter) {
	    return Math.abs(coordinates.getNumber() - this.number);
	}
	return Math.abs(coordinates.getLetter() - this.letter);
    }

    public abstract String encode();

    public boolean equals(Object obj) {
	if (obj == this) {
	    return true;
	}
	if ((obj instanceof Coordinates)) {
	    Coordinates coordinates = (Coordinates) obj;

	    return coordinates.hashCode() == hashCode();
	}
	return false;
    }

    public char getLetter() {
	return this.letter;
    }

    public int getNumber() {
	return this.number;
    }

    public abstract boolean isValid();
}